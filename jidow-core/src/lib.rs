mod color;
mod image;
mod key;
mod pos;

pub use color::Color;
pub use image::Image;
pub use key::MouseButton;
pub use key::{KeyCode, KEY_CODE_COUNT};
pub use pos::Pos;

#[cfg(target_os = "linux")]
mod linux;
#[cfg(target_os = "macos")]
mod macos;
#[cfg(target_os = "windows")]
mod windows;

#[cfg(target_os = "linux")]
use linux as native;
#[cfg(target_os = "macos")]
use macos as native;
#[cfg(target_os = "windows")]
use windows as native;

pub use native::{keyboard, mouse, screen};

pub use keyboard::{down as key_down, input as key_input, press as key_press, up as key_up};
pub use mouse::{
    mv as move_cursor, pos as cursor_pos, set_pos as set_cursor_pos, wheel_down, wheel_up,
};
pub use screen::{
    color as get_color, find_color, find_image, height as screen_height, width as screen_width,
};

pub use mouse_utils::*;
#[rustfmt::skip]
mod mouse_utils {
    use crate::MouseButton;
    use super::mouse;
    pub fn lclick() -> bool { mouse::click(MouseButton::Left) }
    pub fn rclick() -> bool { mouse::click(MouseButton::Right) }
    pub fn mclick() -> bool { mouse::click(MouseButton::Wheel) }

    pub fn lpress() -> bool { mouse::press(MouseButton::Left) }
    pub fn rpress() -> bool { mouse::press(MouseButton::Right) }
    pub fn mpress() -> bool { mouse::press(MouseButton::Wheel) }

    pub fn lrelease() -> bool { mouse::release(MouseButton::Left) }
    pub fn rrelease() -> bool { mouse::release(MouseButton::Right) }
    pub fn mrelease() -> bool { mouse::release(MouseButton::Wheel) }
}

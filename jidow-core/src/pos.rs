#[derive(PartialEq, Eq, PartialOrd, Ord, Clone, Copy, Debug)]
pub struct Pos([usize; 2]);

impl Pos {
    pub fn new(x: usize, y: usize) -> Self {
        Self([x, y])
    }

    pub fn x(self) -> usize {
        unsafe { *self.0.get_unchecked(0) }
    }

    pub fn y(self) -> usize {
        unsafe { *self.0.get_unchecked(1) }
    }
}

impl From<(usize, usize)> for Pos {
    fn from((x, y): (usize, usize)) -> Self {
        Self::new(x, y)
    }
}

impl From<[usize; 2]> for Pos {
    fn from([x, y]: [usize; 2]) -> Self {
        Self::new(x, y)
    }
}

use km::INPUT;
use std::{mem::size_of, mem::MaybeUninit, ptr::addr_of_mut};
use windows_sys::Win32::UI::Input::KeyboardAndMouse::{self as km, SendInput};

pub(super) struct MouseInput(INPUT);
pub(super) struct MouseInputBuilder(MaybeUninit<INPUT>);

impl MouseInput {
    pub fn new() -> MouseInputBuilder {
        let mut input = MaybeUninit::<INPUT>::zeroed();
        unsafe {
            let p_input = input.as_mut_ptr();
            addr_of_mut!((*p_input).r#type).write(km::INPUT_MOUSE);
        }
        MouseInputBuilder(input)
    }

    pub fn send(&self) -> bool {
        unsafe { SendInput(1, &self.0 as _, size_of::<INPUT>() as _) == 1 }
    }
}

impl MouseInputBuilder {
    #[inline(always)]
    fn set_dw_flags(&mut self, f: km::MOUSE_EVENT_FLAGS) {
        unsafe {
            let p_input = self.0.as_mut_ptr();
            addr_of_mut!((*p_input).Anonymous.mi.dwFlags).write(f);
        }
    }

    pub fn left_down(mut self) -> MouseInput {
        self.set_dw_flags(km::MOUSEEVENTF_LEFTDOWN);
        MouseInput(unsafe { self.0.assume_init() })
    }

    pub fn left_up(mut self) -> MouseInput {
        self.set_dw_flags(km::MOUSEEVENTF_LEFTUP);
        MouseInput(unsafe { self.0.assume_init() })
    }

    pub fn right_down(mut self) -> MouseInput {
        self.set_dw_flags(km::MOUSEEVENTF_RIGHTDOWN);
        MouseInput(unsafe { self.0.assume_init() })
    }

    pub fn right_up(mut self) -> MouseInput {
        self.set_dw_flags(km::MOUSEEVENTF_RIGHTUP);
        MouseInput(unsafe { self.0.assume_init() })
    }

    pub fn mid_down(mut self) -> MouseInput {
        self.set_dw_flags(km::MOUSEEVENTF_MIDDLEDOWN);
        MouseInput(unsafe { self.0.assume_init() })
    }

    pub fn mid_up(mut self) -> MouseInput {
        self.set_dw_flags(km::MOUSEEVENTF_MIDDLEUP);
        MouseInput(unsafe { self.0.assume_init() })
    }

    /// A positive value indicates that the wheel was rotated forward,
    /// away from the user; a negative value indicates that the wheel was rotated backward,
    /// toward the user. One wheel click is defined as `WHEEL_DELTA`, which is `120`.
    pub fn wheel(mut self, n: i32) -> MouseInput {
        unsafe {
            let p_input = self.0.as_mut_ptr();
            addr_of_mut!((*p_input).Anonymous.mi.dwFlags).write(km::MOUSEEVENTF_WHEEL);
            addr_of_mut!((*p_input).Anonymous.mi.mouseData).write(n);
        }
        MouseInput(unsafe { self.0.assume_init() })
    }
}

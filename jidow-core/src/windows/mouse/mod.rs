use crate::key::MouseButton::*;
use crate::Pos;
use std::mem::MaybeUninit;
use windows_sys::Win32::Foundation::POINT;
use windows_sys::Win32::UI::WindowsAndMessaging::{GetCursorPos, SetCursorPos};

mod input;
use input::MouseInput as Input;

pub fn release(btn: crate::key::MouseButton) -> bool {
    let input = match btn {
        Left => Input::new().left_up(),
        Right => Input::new().right_up(),
        Wheel => Input::new().mid_up(),
    };
    input.send()
}

pub fn press(btn: crate::key::MouseButton) -> bool {
    let input = match btn {
        Left => Input::new().left_down(),
        Right => Input::new().right_down(),
        Wheel => Input::new().mid_down(),
    };
    input.send()
}

pub fn click(btn: crate::key::MouseButton) -> bool {
    press(btn) && release(btn)
}

pub fn pos() -> Pos {
    let mut point = MaybeUninit::<POINT>::uninit();
    unsafe {
        GetCursorPos(point.as_mut_ptr());
        let point = point.assume_init();
        Pos::new(point.x as usize, point.y as usize)
    }
}

pub fn set_pos(pos: impl Into<Pos>) -> bool {
    let pos = pos.into();
    unsafe { SetCursorPos(pos.x() as i32, pos.y() as i32) != 0 }
}

pub fn mv(dx: i32, dy: i32) -> bool {
    let pos = pos();
    set_pos([
        (pos.x() as i32 + dx) as usize,
        (pos.y() as i32 + dy) as usize,
    ])
}

pub fn wheel_up(count: usize) -> bool {
    Input::new().wheel(count as i32 * 120).send()
}

pub fn wheel_down(count: usize) -> bool {
    Input::new().wheel(count as i32 * -120).send()
}

mod key;
use crate::key::KeyCode;
use key::VirtualKey;
use km::INPUT;
use std::{
    mem::{size_of, MaybeUninit},
    ptr::addr_of_mut,
};
use windows_sys::Win32::UI::Input::KeyboardAndMouse::{self as km, SendInput};

pub(super) enum KeyboardAction {
    KeyDown,
    KeyUp,
}

pub fn up(key: crate::KeyCode) -> bool {
    input_keyboard(key, KeyboardAction::KeyUp)
}

pub fn down(key: crate::KeyCode) -> bool {
    input_keyboard(key, KeyboardAction::KeyDown)
}

pub fn input(text: &str) -> bool {
    text.chars().all(|c| input_unicode_char(c))
}

pub fn press(key: crate::KeyCode) -> bool {
    down(key) && up(key)
}

pub(super) unsafe fn send_input(input: &INPUT) -> bool {
    SendInput(1, input as *const _, size_of::<INPUT>() as i32) == 1
}

pub(super) fn input_keyboard(key: KeyCode, act: KeyboardAction) -> bool {
    unsafe {
        let mut input = MaybeUninit::<INPUT>::zeroed();
        let p_input = input.as_mut_ptr();
        addr_of_mut!((*p_input).r#type).write(km::INPUT_KEYBOARD);
        addr_of_mut!((*p_input).Anonymous.ki.wVk).write(VirtualKey::from(key).inner());
        if let KeyboardAction::KeyUp = act {
            addr_of_mut!((*p_input).Anonymous.ki.dwFlags).write(km::KEYEVENTF_KEYUP);
        }
        send_input(input.assume_init_ref())
    }
}

pub(super) fn input_unicode_char(chr: char) -> bool {
    let input = |up: bool| unsafe {
        let mut input = MaybeUninit::<INPUT>::zeroed();
        let p_input = input.as_mut_ptr();
        addr_of_mut!((*p_input).r#type).write(km::INPUT_KEYBOARD);
        addr_of_mut!((*p_input).Anonymous.ki.wScan).write(chr as u16);
        addr_of_mut!((*p_input).Anonymous.ki.dwFlags)
            .write(km::KEYEVENTF_UNICODE | if up { km::KEYEVENTF_KEYUP } else { 0 });
        send_input(input.assume_init_ref())
    };
    input(false) && input(true)
}

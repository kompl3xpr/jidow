use windows_sys::Win32::{
    Graphics::Gdi::{GetDC, GetPixel, CLR_INVALID},
    UI::WindowsAndMessaging::{GetSystemMetrics, SM_CXSCREEN, SM_CYSCREEN},
};

use crate::{Color, Pos};

pub fn color(pos: impl Into<Pos>) -> Option<Color> {
    unsafe {
        let pos = pos.into();
        let color = GetPixel(GetDC(0), pos.x() as i32, pos.y() as i32);
        if color == CLR_INVALID {
            return None;
        }
        let [r, g, b, _]: [u8; 4] = std::mem::transmute(color);
        Some(Color::rgb(r, g, b))
    }
}

pub fn find_image(_e: &crate::Image, _n: Option<usize>) -> Pos {
    todo!()
}

pub fn find_color(_c: crate::Color, _n: Option<usize>) -> Pos {
    todo!()
}

pub fn width() -> usize {
    unsafe { GetSystemMetrics(SM_CXSCREEN) as usize }
}

pub fn height() -> usize {
    unsafe { GetSystemMetrics(SM_CYSCREEN) as usize }
}

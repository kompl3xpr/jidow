pub const KEY_CODE_COUNT: usize = 12 + 10 + 26 + 16 + 10 + 8 + 10;

#[derive(Clone, Copy, PartialEq, Eq, Debug)]
#[rustfmt::skip]
pub enum KeyCode {
    F1, F2, F3, F4, F5, F6, F7, F8, F9, F10, F11, F12,
    Key1, Key2, Key3, Key4, Key5, Key6, Key7, Key8, Key9, Key0,
    Q, W, E, R, T, Y, U, I, O, P,
    A, S, D, F, G, H, J, K, L,
    Z, X, C, V, B, N, M,

    Escape, Grave, Tab, Capital, LShift, LCtrl, LAlt, Win,
    Space,
    Delete, Back, Return, RShift, RCtrl, RAlt, Numlock,
    
    Numpad0, Numpad1, Numpad2, Numpad3, Numpad4, Numpad5, Numpad6, Numpad7, Numpad8, Numpad9,
    
    Up, Down, Left, Right,
    Home, End, PageUp, PageDown,

    Minus, Equal,
    LBracket, RBracket, Backslash,
    Semicolon, Apostrophe,
    Comma, Period, Slash,
}

#[derive(Clone, Copy, PartialEq, Eq, Debug)]
pub enum MouseButton {
    Left,
    Right,
    Wheel,
}

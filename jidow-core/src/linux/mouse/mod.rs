use crate::Pos;

pub fn release(btn: crate::key::MouseButton) -> bool {
    todo!()
}

pub fn press(btn: crate::key::MouseButton) -> bool {
    todo!()
}

pub fn click(btn: crate::key::MouseButton) -> bool {
    todo!()
}

pub fn pos() -> Pos {
    todo!()
}

pub fn set_pos(pos: impl Into<Pos>) -> bool {
    todo!()
}

pub fn mv(dx: i32, dy: i32) -> bool {
    todo!()
}

pub fn wheel_up(count: usize) -> bool {
    todo!()
}

pub fn wheel_down(count: usize) -> bool {
    todo!()
}

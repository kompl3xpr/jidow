#[derive(PartialEq, Eq, PartialOrd, Ord, Clone, Copy)]
pub struct Color([u8; 4]);

impl Color {
    #[inline(always)]
    pub const fn rgba(r: u8, g: u8, b: u8, a: u8) -> Self {
        Self([r, g, b, a])
    }

    #[inline(always)]
    pub const fn rgb(r: u8, g: u8, b: u8) -> Self {
        Self([r, g, b, 0xFF])
    }

    #[inline(always)]
    pub const fn rgb_from_u32(x: u32) -> Self {
        let [r, g, b, _]: [u8; 4] = unsafe { std::mem::transmute(x) };
        Self::rgb(r, g, b)
    }

    #[inline(always)]
    pub fn r(self) -> u8 {
        unsafe { *self.0.get_unchecked(0) }
    }
    #[inline(always)]
    pub fn g(self) -> u8 {
        unsafe { *self.0.get_unchecked(1) }
    }
    #[inline(always)]
    pub fn b(self) -> u8 {
        unsafe { *self.0.get_unchecked(2) }
    }
}

impl std::fmt::Debug for Color {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "#{:02x}{:02x}{:02x}", self.r(), self.g(), self.b())
    }
}

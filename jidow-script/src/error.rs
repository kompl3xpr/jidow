use std::fmt::{Debug, Display};

use crate::interpreter::error::RuntimeError;
use crate::parser::error::ParseError;
use crate::tokenizer::TokenError;

#[derive(Clone)]
pub enum ScriptError {
    TokenError(TokenError),
    ParseError(ParseError),
    RuntimeError(RuntimeError),
}

impl Display for ScriptError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{:?}", self)
    }
}

impl std::error::Error for ScriptError {}

impl From<TokenError> for ScriptError {
    fn from(e: TokenError) -> Self {
        Self::TokenError(e)
    }
}
impl From<ParseError> for ScriptError {
    fn from(e: ParseError) -> Self {
        Self::ParseError(e)
    }
}
impl From<RuntimeError> for ScriptError {
    fn from(e: RuntimeError) -> Self {
        Self::RuntimeError(e)
    }
}

impl Debug for ScriptError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Self::TokenError(arg0) => f.debug_tuple("TokenError").field(arg0).finish(),
            Self::ParseError(arg0) => f.debug_tuple("ParseError").field(arg0).finish(),
            Self::RuntimeError(arg0) => f.debug_tuple("RuntimeError").field(arg0).finish(),
        }
    }
}

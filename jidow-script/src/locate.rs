use std::fmt::Debug;

/// Something with Location (in which line & column)
pub struct Located<T> {
    inner: T,
    line: usize,
    col: usize,
}

impl<T> Located<T> {
    #[inline]
    pub fn zero(inner: T) -> Self {
        Self::new(inner, 0, 0)
    }

    #[inline]
    pub fn new(inner: T, line: usize, col: usize) -> Self {
        Self { line, col, inner }
    }

    #[inline]
    pub fn as_ref(&self) -> Located<&T> {
        Located::new(&self.inner, self.line(), self.column())
    }

    #[inline]
    pub fn as_mut(&mut self) -> Located<&mut T> {
        let (line, column) = self.location();
        Located::new(&mut self.inner, line, column)
    }

    #[inline]
    pub fn as_inner(&self) -> &T {
        &self.inner
    }

    #[inline]
    pub fn as_inner_mut(&mut self) -> &mut T {
        &mut self.inner
    }

    #[inline]
    pub fn into_inner(self) -> T {
        self.inner
    }

    #[inline]
    pub fn line(&self) -> usize {
        self.line
    }

    #[inline]
    pub fn column(&self) -> usize {
        self.col
    }

    #[inline]
    pub fn location(&self) -> (usize, usize) {
        (self.line(), self.column())
    }
}

impl<T> Located<T> {
    #[inline]
    pub fn map<U>(self, mut f: impl FnMut(T) -> U) -> Located<U> {
        let (line, column) = self.location();
        Located::new(f(self.inner), line, column)
    }

    /// Wraps `other` into a `Located<U>` where the loaction is the same to `self`.
    #[inline]
    pub fn here<U>(&self, other: U) -> Located<U> {
        Located::new(other, self.line(), self.column())
    }

    #[inline]
    pub fn merge<U, V, F>(self, other: Located<U>, mut f: F) -> Located<V>
    where
        F: FnMut(T, U) -> V,
    {
        let (line, column) = min(self.location(), other.location());
        let inner = f(self.inner, other.inner);
        Located::new(inner, line, column)
    }

    #[inline]
    pub fn update<U>(&mut self, other: &Located<U>) {
        (self.line, self.col) = min(self.location(), other.location());
    }
}

impl<T: Clone> Clone for Located<T> {
    fn clone(&self) -> Self {
        Self::new(self.inner.clone(), self.line(), self.column())
    }
}

impl<T: Copy> Copy for Located<T> {}

impl<T: Debug> Debug for Located<T> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "[Ln: {}, Col: {}] {:?}",
            self.line(),
            self.column(),
            self.inner
        )
    }
}

#[inline]
fn min((l1, c1): (usize, usize), (l2, c2): (usize, usize)) -> (usize, usize) {
    if l1 > l2 || (l1 == l2 && c1 > c2) {
        (l2, c2)
    } else {
        (l1, c1)
    }
}

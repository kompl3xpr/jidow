use crate::locate::Located;

use super::{error::ErrorInfo, tokens::Token, TokenError, Tokenizer};

impl<'src> Tokenizer<'src> {
    pub(super) fn next_char(&mut self) -> Option<char> {
        self.skip().map(|chr| {
            self.buf.push(chr);
            chr
        })
    }

    pub(super) fn ignore_last(&mut self) {
        self.buf.pop();
    }

    pub(super) fn replace_last(&mut self, chr: char) {
        self.ignore_last();
        self.buf.push(chr);
    }

    pub(super) fn skip(&mut self) -> Option<char> {
        self.chars.next().map(|chr| {
            match chr {
                '\n' => (self.line, self.col, self.last_col) = (self.line + 1, 0, self.col),
                _ => self.col += 1,
            }
            chr
        })
    }

    pub(super) fn peek_char(&mut self) -> Option<char> {
        self.chars.peek().map(|x| *x)
    }

    pub(super) fn success(&mut self, token: Token) -> Located<Token> {
        let d_col = self.buf.len();
        self.buf.clear();
        let (line, col) = match d_col <= self.col {
            true => (self.line, self.col - d_col),
            _ => (self.line - 1, self.last_col),
        };
        Located::<Token>::new(token, line, col)
    }

    pub(super) fn success_then<F>(&mut self, into_token: F) -> Result<Located<Token>, TokenError>
    where
        F: FnOnce(String) -> Result<Token, ErrorInfo>,
    {
        let token = std::mem::replace(&mut self.buf, vec![]);
        let d_col = token.len();
        let (line, col) = match d_col <= self.col {
            true => (self.line, self.col - d_col),
            _ => (self.line - 1, self.last_col),
        };
        into_token(token.into_iter().collect())
            .map_err(|t| self.fail(t))
            .map(|t| Located::<Token>::new(t, line, col))
    }

    pub(super) fn fail(&mut self, info: ErrorInfo) -> TokenError {
        self.buf.clear();
        TokenError(Located::new(info, self.line, self.col))
    }

    pub(super) fn remove_buf(&mut self) -> Vec<char> {
        std::mem::replace(&mut self.buf, vec![])
    }
}

pub mod error;
pub mod tokens;
pub use error::TokenError;

const INDENT: i32 = 4;

use crate::locate::Located;

use self::{
    error::ErrorInfo::{self, *},
    other::SpaceState,
    tokens::Token,
};
use std::{iter::Peekable, str::Chars};

pub struct Tokenizer<'src> {
    chars: Peekable<Chars<'src>>,
    buf: Vec<char>,
    line: usize,
    col: usize,
    last_col: usize,
}

pub(super) mod duration;
pub(super) mod key_ident;
pub(super) mod numeric;
pub(super) mod other;
pub(super) mod punct;
pub(super) mod utils;

impl<'a> Tokenizer<'a> {
    pub fn new(text: &'a str) -> Self {
        Self {
            chars: text.chars().peekable(),
            buf: Vec::new(),
            line: 1,
            col: 0,
            last_col: 0,
        }
    }

    pub fn try_tokenize(&mut self) -> Result<Vec<Located<Token>>, TokenError> {
        let mut tokens = vec![Located::<Token>::new(Token::ScopeBegin, 0, 0)];
        let mut last_indent: Option<Located<Token>> = None;
        let mut indent_stack: Vec<i32> = vec![0];

        while let Some(result) = self.next_token() {
            let token = result?;
            match token.as_inner() {
                Token::Space => continue,
                Token::Indent(_) => {
                    last_indent = Some(token);
                    continue;
                }
                _ => (),
            }

            let indent = match last_indent.take() {
                None => {
                    tokens.push(token);
                    continue;
                }
                Some(i) => i,
            };

            tokens.push(indent.here(Token::NextLn));
            let n = match indent.as_inner() {
                Token::Indent(n) => *n,
                _ => unreachable!(),
            };
            let mut offset = n as i32 - indent_stack.last().unwrap();
            while offset.abs() >= INDENT {
                if offset.is_positive() {
                    indent_stack.push(n as i32);
                    tokens.push(indent.here(Token::ScopeBegin));
                    offset -= 4;
                } else {
                    indent_stack.pop();
                    tokens.push(indent.here(Token::ScopeEnd));
                    tokens.push(indent.here(Token::NextLn));
                    offset += 4;
                }
            }
            if offset != 0 {
                return Err(self.fail(IndentNotWithFourBlank));
            }
            tokens.push(token)
        }
        let last = tokens.last().unwrap().here(());
        while let Some(_) = indent_stack.pop() {
            tokens.push(last.here(Token::NextLn));
            tokens.push(last.here(Token::ScopeEnd));
        }
        Ok(tokens)
    }

    pub(super) fn next_token(&mut self) -> Option<Result<Located<Token>, TokenError>> {
        let result = match self.next_char()? {
            '0' => self.numeric_state(numeric::State::Zero),
            c if c.is_digit(10) => self.numeric_state(numeric::State::Dec),

            '~' => self.comment_state(),
            '#' => self.hexcolor_state(),
            '\"' => self.string_state(),

            c if c.is_ascii_punctuation() && c != '_' => self.punct_state(c),
            '\n' => self.space_state(SpaceState::Indent(0)),
            ' ' | '\t' => self.space_state(SpaceState::Other),

            c if c.is_ascii_alphabetic() || c == '_' || !c.is_ascii() => {
                self.keyword_or_ident_state(c)
            }
            c => return Some(Err(self.fail(UnexpectedCharInBeginState(c)))),
        };
        Some(result)
    }
}

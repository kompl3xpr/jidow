use std::time::Duration;

#[derive(Debug, Clone, PartialEq)]
pub enum Token {
    Space,
    Indent(u32),

    NextLn,
    ScopeBegin,
    ScopeEnd,

    Ident(String),
    Str(String),
    Integer(u32),
    Float(f64),
    HexColor(u32),
    Duration(Duration),
    Keyword(Keyword),
    Punct(Punct),
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum Keyword {
    And,
    Be,
    Break,
    Continue,
    Do,
    Else,
    False,
    For,
    Func,
    If,
    In,
    Let,
    Not,
    Or,
    Return,
    Self_,
    Set,
    That,
    To,
    True,
    While,
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum Punct {
    Colon,
    DoubleMinus,
    Asterisk,
    Slash,
    Eq, // =
    Lt,
    Gt,
    LParen,
    RParen,
    LBracket, // [
    RBracket,
    LBrace, // {
    RBrace,
    Plus,
    Minus,
    Leq,
    Geq,
    Percent,
    Caret,
    Period,
    Comma,
}

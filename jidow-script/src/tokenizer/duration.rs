use super::{TokenError, Tokenizer};
use crate::{
    locate::Located,
    tokenizer::{
        error::ErrorInfo::{self, *},
        tokens::Token,
    },
};
use std::time::Duration;

enum State {
    N,

    SecNM,
    MinNM,

    SecN,
    MinN,
    HourN,

    NHour,
    NMin,
    NSec,
    NMilis,
}
use State::*;

impl<'src> Tokenizer<'src> {
    pub(super) fn duration_state(&mut self) -> Result<Located<Token>, TokenError> {
        let mut state = State::N;
        let [mut hour, mut min, mut sec, mut ms] = [0, 0, 0, 0];

        fn parse(s: Vec<char>, end: &str) -> Result<u64, ErrorInfo> {
            let s: String = s.into_iter().collect();
            let mut trimmed = s.as_str();
            for chr in end.chars().rev() {
                trimmed = trimmed.trim_end_matches(chr);
            }
            trimmed
                .parse::<u64>()
                .map_err(|_| FailedToParseInteger(trimmed.into()))
        }

        while let Some(chr) = self.peek_char() {
            let mut error = |c| Err(self.fail(UnexpectedCharInDurationState(c)));
            state = match chr {
                '0'..='9' => match state {
                    SecNM | MinNM | NMilis => return error(chr),
                    N => N,
                    NHour | NMin | NSec => {
                        let (var, next, end) = match state {
                            NHour => (&mut hour, HourN, "h"),
                            NMin => (&mut min, MinN, "m"),
                            NSec => (&mut sec, SecN, "s"),
                            _ => unreachable!(),
                        };
                        *var = parse(self.remove_buf(), end).map_err(|e| self.fail(e))?;
                        next
                    }
                    xxx_n => xxx_n,
                },
                'h' => match state {
                    N => NHour,
                    _ => return error(chr),
                },
                'm' => match state {
                    N => NMin,
                    HourN => NMin,
                    MinN => MinNM,
                    SecN => SecNM,
                    _ => return error(chr),
                },
                's' => match state {
                    N => NSec,
                    HourN | MinN => NSec,
                    NMin | SecNM | MinNM => NMilis,
                    _ => return error(chr),
                },
                c if c.is_ascii_alphabetic() || c == '_' || !c.is_ascii() => return error(chr),
                _ => break,
            };
            self.next_char();
        }

        match state {
            NHour | NMin | NSec | NMilis => {
                let (var, end) = match state {
                    NHour => (&mut hour, "h"),
                    NMin => (&mut min, "m"),
                    NSec => (&mut sec, "s"),
                    NMilis => (&mut ms, "ms"),
                    _ => unreachable!(),
                };
                *var = parse(self.remove_buf(), end).map_err(|e| self.fail(e))?;
                let mut dur = Duration::from_millis(ms);
                dur += Duration::from_secs(3600 * hour + 60 * min + sec);
                Ok(self.success(Token::Duration(dur)))
            }
            _ => return Err(self.fail(EmptyInDurationState)),
        }
    }
}

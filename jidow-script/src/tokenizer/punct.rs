use crate::locate::Located;

use super::{
    tokens::{
        Punct::*,
        Token::{self, *},
    },
    ErrorInfo::*,
    TokenError, Tokenizer,
};

impl<'src> Tokenizer<'src> {
    pub(super) fn punct_state(&mut self, init: char) -> Result<Located<Token>, TokenError> {
        let mut state = String::from(init);
        while let Some(chr) = self.peek_char() {
            match chr {
                '=' => match state.as_str() {
                    ">" | "<" => state.push('='),
                    _ => break,
                },
                '-' => match state.as_str() {
                    "-" => state.push('-'),
                    _ => break,
                },
                _ => break,
            }
            self.next_char();
        }

        match state.as_str() {
            "<=" => Ok(self.success(Punct(Leq))),
            ">=" => Ok(self.success(Punct(Geq))),
            "--" => Ok(self.success(Punct(DoubleMinus))),
            "*" => Ok(self.success(Punct(Asterisk))),
            "/" => Ok(self.success(Punct(Slash))),
            "=" => Ok(self.success(Punct(Eq))),
            "<" => Ok(self.success(Punct(Lt))),
            ">" => Ok(self.success(Punct(Gt))),
            "(" => Ok(self.success(Punct(LParen))),
            ")" => Ok(self.success(Punct(RParen))),
            "[" => Ok(self.success(Punct(LBracket))),
            "]" => Ok(self.success(Punct(RBracket))),
            "{" => Ok(self.success(Punct(LBrace))),
            "}" => Ok(self.success(Punct(RBrace))),
            "+" => Ok(self.success(Punct(Plus))),
            "-" => Ok(self.success(Punct(Minus))),
            "%" => Ok(self.success(Punct(Percent))),
            "^" => Ok(self.success(Punct(Caret))),
            "." => Ok(self.success(Punct(Period))),
            "," => Ok(self.success(Punct(Comma))),
            ":" => Ok(self.success(Punct(Colon))),
            _ => Err(self.fail(UnexpectedCharInPunctuationState(init))),
        }
    }
}

#![allow(unused)]

use crate::locate::Located;

use super::{
    tokens::{
        Keyword::*,
        Token::{self, *},
    },
    TokenError, Tokenizer,
};

#[rustfmt::skip]
#[allow(non_camel_case_types)]
pub(super) enum State {
    A, B, C, D, E, F, I, L, N, O, R, S, T, W,
    AN, AND,
    BE,
    BR, BRE, BREA, BREAK,
    CO, CON, CONT, CONTI, CONTIN, CONTINU, CONTINUE,
    DO,
    EL, ELS, ELSE,
    FA, FAL, FALS, FALSE,
    FO, FOR,
    FU, FUN, FUNC,
    IF,
    IN,
    LE, LET,
    NO, NOT,
    OR,
    RE, RET, RETU, RETUR, RETURN,
    SE, SET,
    SEL, SELF,
    TH, THA, THAT,
    TO,
    TR, TRU, TRUE,
    WH, WHI, WHIL, WHILE,
}

use State::*;

macro_rules! next_state {
    ([$self: ident $state: ident $chr: ident] $($s: ident => $(!)? $($c: literal $next: ident)|*,)+) => {
        match $state {
            $($s => match $chr { $($c => $next,)* _ => return $self.ident_state() })+
        }
    };
}

impl<'src> Tokenizer<'src> {
    #[rustfmt::skip]
    pub(super) fn keyword_or_ident_state(&mut self, init: char) -> Result<Located<Token>, TokenError> {
        let mut state = match init {
            'a' => A,
            'b' => B,
            'c' => C,
            'd' => D,
            'e' => E,
            'f' => F,
            'i' => I,
            'l' => L,
            'n' => N,
            'o' => O,
            'r' => R,
            's' => S,
            't' => T,
            'w' => W,
            _ => return self.ident_state(),
        };

        while let Some(chr) = self.peek_char() {
            if chr.is_whitespace() || (chr.is_ascii_punctuation() && chr != '_') {
                break;
            }
            state = next_state!{[self state chr]
                A => 'n' AN,            B => 'e' BE | 'r' BR,
                C => 'o' CO,            D => 'o' DO,
                E => 'l' EL,            F => 'o' FO | 'a' FA | 'u' FU,
                I => 'n' IN | 'f' IF,   L => 'e' LE,
                N => 'o' NO,            O => 'r' OR,
                R => 'e' RE,            S => 'e' SE,
                T => 'h' TH | 'o' TO | 'r' TR,   W => 'h' WH,

                SE => 't' SET | 'l' SEL,

                AND => !, AN => 'd' AND,
                BE => !,
                BREAK => !, BR => 'e' BRE, BRE => 'a' BREA, BREA => 'k' BREAK,
                CONTINUE => !, CO => 'n' CON, CON => 't' CONT, CONT => 'i' CONTI, CONTI => 'n' CONTIN, CONTIN => 'u' CONTINU, CONTINU => 'e' CONTINUE,
                DO => !,
                ELSE => !, EL => 's' ELS, ELS => 'e' ELSE,
                FALSE => !, FA => 'l' FAL, FAL => 's' FALS, FALS => 'e' FALSE,
                FOR => !, FO => 'r' FOR,
                FUNC => !, FU => 'n' FUN, FUN => 'c' FUNC,
                IF => !,
                IN => !,
                LET => !, LE => 't' LET,
                NOT => !, NO => 't' NOT,
                OR => !,
                RETURN => !, RE => 't' RET, RET => 'u' RETU, RETU => 'r' RETUR, RETUR => 'n' RETURN,
                SET => !,
                SELF => !, SEL => 'f' SELF,
                THAT => !, TH => 'a' THA, THA => 't' THAT,
                TO => !,
                TRUE => !, TR => 'u' TRU, TRU => 'e' TRUE,
                WHILE => !, WH => 'i' WHI, WHI => 'l' WHIL, WHIL => 'e' WHILE,
            };
            self.next_char();
        }

        match state {
            AND => Ok(self.success(Keyword(And))),
            BE => Ok(self.success(Keyword(Be))),
            BREAK => Ok(self.success(Keyword(Break))),
            CONTINUE => Ok(self.success(Keyword(Continue))),
            FUNC => Ok(self.success(Keyword(Func))),
            DO => Ok(self.success(Keyword(Do))),
            ELSE => Ok(self.success(Keyword(Else))),
            FALSE => Ok(self.success(Keyword(False))),
            FOR => Ok(self.success(Keyword(For))),
            IF => Ok(self.success(Keyword(If))),
            IN => Ok(self.success(Keyword(In))),
            LET => Ok(self.success(Keyword(Let))),
            NOT => Ok(self.success(Keyword(Not))),
            OR => Ok(self.success(Keyword(Or))),
            RETURN => Ok(self.success(Keyword(Return))),
            SET => Ok(self.success(Keyword(Set))),
            SELF => Ok(self.success(Keyword(Self_))),
            THAT => Ok(self.success(Keyword(That))),
            TO => Ok(self.success(Keyword(To))),
            TRUE => Ok(self.success(Keyword(True))),
            WHILE => Ok(self.success(Keyword(While))),
            _ => self.success_then(|s| Ok(Ident(s))),
        }
    }

    pub(super) fn ident_state(&mut self) -> Result<Located<Token>, TokenError> {
        while let Some(chr) = self.peek_char() {
            if chr.is_whitespace() || (chr.is_ascii_punctuation() && chr != '_') {
                break;
            }
            self.next_char();
        }
        self.success_then(|s| Ok(Ident(s)))
    }
}

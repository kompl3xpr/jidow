use std::{
    error::Error,
    fmt::{Debug, Display},
};

use crate::locate::Located;

#[derive(Clone)]
pub struct TokenError(pub Located<ErrorInfo>);

#[derive(Debug, PartialEq, Eq, Clone)]
pub enum ErrorInfo {
    UnexpectedCharInBeginState(char),

    UnexpectedCharInNumericState(char),
    FailedToParseFloat(String),
    FailedToParseInteger(String),

    UnexpectedCharInPunctuationState(char),

    InvalidCharToEscapeInString(char),
    EmptyInStringState,

    EmptyInHexColorState,
    UnexpectedCharInHexColorState(char),

    EmptyInDurationState,
    UnexpectedCharInDurationState(char),

    TabAsIndent,
    IndentNotWithFourBlank,
}

impl Debug for TokenError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        self.0.fmt(f)
    }
}

impl Display for TokenError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{:?}", self.0)
    }
}

impl Error for TokenError {}

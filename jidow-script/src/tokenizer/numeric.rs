use super::{tokens::Token, ErrorInfo::*, TokenError, Tokenizer};
use crate::{locate::Located, tokenizer::error::ErrorInfo};

pub(super) enum State {
    Zero,
    Dec,
    Float,
    Hex,
    Bin,
}

impl<'src> Tokenizer<'src> {
    pub(super) fn numeric_state(&mut self, init: State) -> Result<Located<Token>, TokenError> {
        let mut state = init;
        while let Some(c) = self.peek_char() {
            let mut error = |c| Err(self.fail(UnexpectedCharInNumericState(c)));
            state = match c {
                '.' => match state {
                    State::Dec | State::Zero => State::Float,
                    _ => return error(c),
                },
                '0' => match state {
                    State::Float => State::Float,
                    State::Zero => State::Dec,
                    dec_bin_hex => dec_bin_hex,
                },
                '1' => match state {
                    State::Float => State::Float,
                    State::Zero => State::Dec,
                    dec_bin_hex => dec_bin_hex,
                },
                'b' | 'B' => match state {
                    State::Zero => State::Bin,
                    State::Hex => State::Hex,
                    _ => return error(c),
                },
                'x' | 'X' => match state {
                    State::Zero => State::Hex,
                    _ => return error(c),
                },
                '2'..='9' => match state {
                    State::Zero => State::Dec,
                    State::Bin => return error(c),
                    dec_hex_float => dec_hex_float,
                },
                'h' | 'm' | 's' => match state {
                    State::Zero | State::Dec => return self.duration_state(),
                    _ => return error(c),
                },
                c if c.is_ascii_hexdigit() => match state {
                    State::Hex => State::Hex,
                    _ => return error(c),
                },
                _ => break,
            };
            self.next_char();
        }

        match state {
            State::Zero | State::Dec => return self.success_then(parse_dec),
            State::Float => return self.success_then(parse_float),
            State::Hex => return self.success_then(parse_hex),
            State::Bin => return self.success_then(parse_bin),
        }
    }
}

fn parse_float(s: String) -> Result<Token, ErrorInfo> {
    s.parse::<f64>()
        .map(|n| Token::Float(n))
        .map_err(|_| FailedToParseFloat(s))
}

fn parse_dec(s: String) -> Result<Token, ErrorInfo> {
    s.parse::<u32>()
        .map(|n| Token::Integer(n))
        .map_err(|_| FailedToParseInteger(s))
}

fn parse_hex(s: String) -> Result<Token, ErrorInfo> {
    u32::from_str_radix(s.trim_start_matches('0').trim_start_matches(['x', 'X']), 16)
        .map(|n| Token::Integer(n))
        .map_err(|_| FailedToParseInteger(s))
}

fn parse_bin(s: String) -> Result<Token, ErrorInfo> {
    u32::from_str_radix(s.trim_start_matches('0').trim_start_matches(['b', 'B']), 2)
        .map(|n| Token::Integer(n))
        .map_err(|_| FailedToParseInteger(s))
}

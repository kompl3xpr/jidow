use super::{tokens::Token, ErrorInfo::*, TokenError, Tokenizer, INDENT};
use crate::locate::Located;

pub(super) enum SpaceState {
    Indent(u32),
    Other,
}

impl<'src> Tokenizer<'src> {
    pub(super) fn string_state(&mut self) -> Result<Located<Token>, TokenError> {
        enum State {
            Open,
            Close,
            Escape,
        }
        self.ignore_last();
        let mut state = State::Open;
        while let Some(chr) = self.peek_char() {
            state = match state {
                State::Close => break,
                State::Open => match chr {
                    '\\' => State::Escape,
                    '\"' => State::Close,
                    _ => State::Open,
                },
                State::Escape => {
                    match chr {
                        '\"' | '\'' => self.replace_last(chr),
                        '\\' => (),
                        'n' => self.replace_last('\n'),
                        't' => self.replace_last('\t'),
                        'r' => self.replace_last('\r'),
                        _ => return Err(self.fail(InvalidCharToEscapeInString(chr))),
                    }
                    state = State::Open;
                    self.skip();
                    continue;
                }
            };
            self.next_char();
        }
        match state {
            State::Close => {
                self.ignore_last();
                self.success_then(|s| Ok(Token::Str(s)))
            }
            State::Open => Err(self.fail(EmptyInStringState)),
            State::Escape => Err(self.fail(EmptyInStringState)),
        }
    }

    pub(super) fn space_state(&mut self, init: SpaceState) -> Result<Located<Token>, TokenError> {
        let mut state = init;
        while let Some(chr) = self.peek_char() {
            state = match chr {
                ' ' => match state {
                    SpaceState::Indent(n) => SpaceState::Indent(n + 1),
                    SpaceState::Other => SpaceState::Other,
                },
                '\t' => match state {
                    SpaceState::Indent(n) => SpaceState::Indent(n + INDENT as u32),
                    SpaceState::Other => SpaceState::Other,
                },
                '\n' => match state {
                    SpaceState::Other => SpaceState::Indent(0),
                    SpaceState::Indent(_) => break,
                },
                _ => break,
            };
            self.next_char();
        }
        match state {
            SpaceState::Indent(n) => Ok(self.success(Token::Indent(n))),
            SpaceState::Other => Ok(self.success(Token::Space)),
        }
    }

    pub(super) fn hexcolor_state(&mut self) -> Result<Located<Token>, TokenError> {
        self.ignore_last();
        let mut state: usize = 0;
        while let Some(chr) = self.peek_char() {
            if chr.is_ascii_hexdigit() {
                if state == 6 {
                    return Err(self.fail(UnexpectedCharInHexColorState(chr)));
                }
                state += 1;
                self.next_char();
                continue;
            } else if chr.is_ascii_alphabetic() || chr == '_' || !chr.is_ascii() {
                return Err(self.fail(UnexpectedCharInHexColorState(chr)));
            }
            break;
        }
        if state != 6 {
            return Err(self.fail(EmptyInHexColorState));
        }
        self.success_then(|s| Ok(Token::HexColor(u32::from_str_radix(&s, 16).unwrap())))
    }

    pub(super) fn comment_state(&mut self) -> Result<Located<Token>, TokenError> {
        while let Some(chr) = self.peek_char() {
            if chr == '\n' {
                break;
            }
            self.next_char();
        }
        Ok(self.success(Token::Space))
    }
}

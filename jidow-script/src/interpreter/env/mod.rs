use super::{expr::Value, Interpreter};
use std::collections::{HashMap, LinkedList};

mod global;
pub(super) use global::GLOBAL;

mod input;
mod utils;

#[derive(Clone)]
pub struct Env<'a> {
    frame: Vec<Vec<&'a str>>,
    table: HashMap<&'a str, LinkedList<Value<'a>>>,
}

impl<'a> Env<'a> {
    pub fn new() -> Self {
        let frame = vec![];
        let table = HashMap::new();
        Self { frame, table }
    }

    fn join_scope(&mut self) {
        self.frame.push(vec![]);
    }

    fn leave_scope(&mut self) {
        self.frame.pop().map(|symbols| {
            symbols.into_iter().for_each(|symbol| {
                self.table.get_mut(symbol).map(LinkedList::pop_back);
            })
        });
    }

    pub fn get(&self, symbol: &str) -> Option<&Value<'a>> {
        GLOBAL
            .get(symbol)
            .or(self.table.get(symbol).and_then(LinkedList::back))
    }

    pub fn get_mut(&mut self, symbol: &str) -> Result<Option<&mut Value<'a>>, ()> {
        if GLOBAL.contains_key(symbol) {
            return Err(());
        }
        Ok(self.table.get_mut(symbol).and_then(LinkedList::back_mut))
    }

    pub fn insert(&mut self, symbol: &'a str, val: Value<'a>) {
        self.frame.last_mut().map(|vec| vec.push(symbol));
        match self.table.get_mut(symbol) {
            Some(list) => list.push_back(val),
            None => {
                self.table.insert(symbol, LinkedList::from_iter([val]));
            }
        }
    }
}

pub struct Scope<'scp, 'ast>(&'scp mut Interpreter<'ast>);

impl<'scp, 'ast> Scope<'scp, 'ast> {
    pub(super) fn new(interpreter: &'scp mut Interpreter<'ast>) -> Self {
        interpreter.env.join_scope();
        Self(interpreter)
    }
}

impl<'scp, 'ast> std::ops::Drop for Scope<'scp, 'ast> {
    fn drop(&mut self) {
        self.0.env.leave_scope();
    }
}

impl<'scp, 'ast> std::ops::Deref for Scope<'scp, 'ast> {
    type Target = Interpreter<'ast>;

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

impl<'scp, 'ast> std::ops::DerefMut for Scope<'scp, 'ast> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}

use crate::{interpreter::expr::Value, OUTPUT_SENDER};

pub fn print(args: Vec<Value>) -> Value {
    if let Some(sender) = OUTPUT_SENDER.get() {
        let output = args
            .iter()
            .fold(String::new(), |s, x| s + &format!("{:?} ", x));
        sender.send(output + "\n").ok();
    }
    Value::Null
}

pub fn fmt(args: Vec<Value>) -> Value {
    let mut args = args.into_iter();
    if let Some(Value::Str(mut f)) = args.next() {
        args.for_each(|arg| f = f.replacen("{}", &format!("{:?}", arg), 1));
        return Value::Str(f);
    }
    Value::Null
}

pub fn range(args: Vec<Value>) -> Value {
    if let (Some(Value::Integer(begin)), Some(Value::Integer(end))) = (args.get(0), args.get(1)) {
        let step = match args.get(2) {
            Some(Value::Integer(step)) => Some(*step),
            _ => None,
        };
        return Value::Range(*begin, *end, step);
    }
    Value::Null
}

use super::{input::*, utils::*};
use crate::interpreter::expr::{
    Object,
    Value::{self, KeyCode as Key},
};
use jidow_core::{self as core, KeyCode::*};
use once_cell::sync::Lazy;
use std::collections::HashMap;

pub static GLOBAL: Lazy<HashMap<&'static str, Value>> = Lazy::new(|| {
    HashMap::from_iter([
        ("F1", Key(F1)),
        ("F2", Key(F1)),
        ("F3", Key(F1)),
        ("F4", Key(F1)),
        ("F5", Key(F1)),
        ("F6", Key(F1)),
        ("F7", Key(F1)),
        ("F8", Key(F1)),
        ("F9", Key(F1)),
        ("F10", Key(F1)),
        ("F11", Key(F1)),
        ("F12", Key(F1)),
        ("KEY1", Key(Key1)),
        ("KEY2", Key(Key2)),
        ("KEY3", Key(Key3)),
        ("KEY4", Key(Key4)),
        ("KEY5", Key(Key5)),
        ("KEY6", Key(Key6)),
        ("KEY7", Key(Key7)),
        ("KEY8", Key(Key8)),
        ("KEY9", Key(Key9)),
        ("KEY0", Key(Key0)),
        ("A", Key(A)),
        ("B", Key(B)),
        ("C", Key(C)),
        ("D", Key(D)),
        ("E", Key(E)),
        ("F", Key(F)),
        ("G", Key(G)),
        ("H", Key(H)),
        ("I", Key(I)),
        ("J", Key(J)),
        ("K", Key(K)),
        ("L", Key(L)),
        ("M", Key(M)),
        ("N", Key(N)),
        ("O", Key(O)),
        ("P", Key(P)),
        ("Q", Key(Q)),
        ("R", Key(R)),
        ("S", Key(S)),
        ("T", Key(T)),
        ("U", Key(U)),
        ("V", Key(V)),
        ("W", Key(W)),
        ("X", Key(X)),
        ("Y", Key(Y)),
        ("Z", Key(Z)),
        ("ESCAPE", Key(Escape)),
        ("GRAVE", Key(Grave)),
        ("TAB", Key(Tab)),
        ("CAPITAL", Key(Capital)),
        ("LSHIFT", Key(LShift)),
        ("LCTRL", Key(LCtrl)),
        ("LALT", Key(LAlt)),
        ("WIN", Key(Win)),
        ("SPACE", Key(Space)),
        ("DELETE", Key(Delete)),
        ("BACK", Key(Back)),
        ("RETURN", Key(Return)),
        ("RSHIFT", Key(RShift)),
        ("RCTRL", Key(RCtrl)),
        ("RALT", Key(RAlt)),
        ("NUMLOCK", Key(Numlock)),
        ("NUMPAD1", Key(Numpad1)),
        ("NUMPAD2", Key(Numpad2)),
        ("NUMPAD3", Key(Numpad3)),
        ("NUMPAD4", Key(Numpad4)),
        ("NUMPAD5", Key(Numpad5)),
        ("NUMPAD6", Key(Numpad6)),
        ("NUMPAD7", Key(Numpad7)),
        ("NUMPAD8", Key(Numpad8)),
        ("NUMPAD9", Key(Numpad9)),
        ("NUMPAD0", Key(Numpad0)),
        ("UP", Key(Up)),
        ("DOWN", Key(Down)),
        ("LEFT", Key(Left)),
        ("RIGHT", Key(Right)),
        ("HOME", Key(Home)),
        ("END", Key(End)),
        ("PAGEUP", Key(PageUp)),
        ("PAGEDOWN", Key(PageDown)),
        ("MINUS", Key(Minus)),
        ("EQUAL", Key(Equal)),
        ("LBRACKET", Key(LBracket)),
        ("RBRACKET", Key(RBracket)),
        ("BACKSLASH", Key(Backslash)),
        ("SEMICOLON", Key(Semicolon)),
        ("APOSTROPHE", Key(Apostrophe)),
        ("COMMA", Key(Comma)),
        ("PERIOD", Key(Period)),
        ("SLASH", Key(Slash)),
        ("Cursor", Value::Object(&Cursor)),
        ("Screen", Value::Object(&Screen)),
        ("sleep", Value::BuiltinFunc(&sleep)),
        ("print", Value::BuiltinFunc(&print)),
        ("key_down", Value::BuiltinFunc(&key_down)),
        ("key_up", Value::BuiltinFunc(&key_up)),
        ("key_input", Value::BuiltinFunc(&key_input)),
        ("key_press", Value::BuiltinFunc(&key_press)),
        ("lclick", Value::BuiltinFunc(&lclick)),
        ("rclick", Value::BuiltinFunc(&rclick)),
        ("mclick", Value::BuiltinFunc(&mclick)),
        ("lpress", Value::BuiltinFunc(&lpress)),
        ("rpress", Value::BuiltinFunc(&rpress)),
        ("mpress", Value::BuiltinFunc(&mpress)),
        ("lrelease", Value::BuiltinFunc(&lrelease)),
        ("rrelease", Value::BuiltinFunc(&rrelease)),
        ("mrelease", Value::BuiltinFunc(&mrelease)),
        ("wheel_down", Value::BuiltinFunc(&wheel_down)),
        ("wheel_up", Value::BuiltinFunc(&wheel_up)),
        ("range", Value::BuiltinFunc(&range)),
        ("fmt", Value::BuiltinFunc(&fmt)),
    ])
});

struct Cursor;
impl Object for Cursor {
    fn access(&self, member: &str) -> Option<Value> {
        match member {
            "position" => {
                let pos = core::cursor_pos();
                Some(Value::Array(vec![
                    Value::Integer(pos.x() as i32),
                    Value::Integer(pos.y() as i32),
                ]))
            }
            "set_position" => Some(Value::BuiltinFunc(&set_cursor_pos)),
            "move" => Some(Value::BuiltinFunc(&move_cursor)),
            _ => None,
        }
    }
}

struct Screen;
impl Object for Screen {
    fn access(&self, member: &str) -> Option<Value> {
        match member {
            "width" => Some(Value::Integer(core::screen_width() as i32)),
            "height" => Some(Value::Integer(core::screen_height() as i32)),
            "color" => Some(Value::BuiltinFunc(&get_color)),
            "find_color" => Some(Value::BuiltinFunc(&find_color)),
            "find_image" => Some(Value::BuiltinFunc(&find_image)),
            _ => None,
        }
    }
}

use crate::interpreter::expr::{Func, Value};
use jidow_core as core;

impl<F: Fn(Vec<Value>) -> Value + Sync + Send> Func for F {
    fn call<'a>(&self, args: Vec<Value<'a>>) -> Value<'a> {
        self(args)
    }
}

pub fn key_down(args: Vec<Value>) -> Value {
    if let Some(Value::KeyCode(key)) = args.get(0) {
        return Value::Boolean(core::key_down(*key));
    }
    Value::Null
}

pub fn key_up(args: Vec<Value>) -> Value {
    if let Some(Value::KeyCode(key)) = args.get(0) {
        return Value::Boolean(core::key_up(*key));
    }
    Value::Null
}

pub fn key_input(args: Vec<Value>) -> Value {
    if let Some(Value::Str(s)) = args.get(0) {
        return Value::Boolean(core::key_input(s));
    }
    Value::Null
}

pub fn key_press(args: Vec<Value>) -> Value {
    if let Some(Value::KeyCode(key)) = args.get(0) {
        return Value::Boolean(core::key_press(*key));
    }
    Value::Null
}

pub fn move_cursor(args: Vec<Value>) -> Value {
    if let (Some(Value::Integer(dx)), Some(Value::Integer(dy))) = (args.get(0), args.get(1)) {
        return Value::Boolean(core::move_cursor(*dx, *dy));
    }
    Value::Null
}

pub fn set_cursor_pos(args: Vec<Value>) -> Value {
    if let Some(Value::Array(pos)) = args.get(0) {
        if let (Some(Value::Integer(x)), Some(Value::Integer(y))) = (pos.get(0), pos.get(1)) {
            let result = core::set_cursor_pos([*x as usize, *y as usize]);
            return Value::Boolean(result);
        }
    }
    Value::Null
}

pub fn wheel_down(args: Vec<Value>) -> Value {
    if let Some(Value::Integer(n)) = args.get(0) {
        return Value::Boolean(core::wheel_down(*n as usize));
    }
    Value::Null
}

pub fn wheel_up(args: Vec<Value>) -> Value {
    if let Some(Value::Integer(n)) = args.get(0) {
        return Value::Boolean(core::wheel_up(*n as usize));
    }
    Value::Null
}

pub fn get_color(args: Vec<Value>) -> Value {
    if let Some(Value::Array(pos)) = args.get(0) {
        if let (Some(Value::Integer(x)), Some(Value::Integer(y))) = (pos.get(0), pos.get(1)) {
            let result = core::get_color([*x as usize, *y as usize]);
            return match result {
                Some(color) => Value::Color(color),
                None => Value::Null,
            };
        }
    }
    Value::Null
}

pub fn find_color(_: Vec<Value>) -> Value {
    todo!()
}

pub fn find_image(_: Vec<Value>) -> Value {
    todo!()
}

fn mouse_action(args: Vec<Value>, action: fn() -> bool) -> Value {
    let times = match args.get(0) {
        Some(Value::Integer(i)) => *i as usize,
        Some(_) => return Value::Null,
        None => 1,
    };
    Value::Boolean((0..times).map(|_| action()).all(|x| x))
}

pub fn lclick(args: Vec<Value>) -> Value {
    mouse_action(args, core::lclick)
}
pub fn rclick(args: Vec<Value>) -> Value {
    mouse_action(args, core::rclick)
}
pub fn mclick(args: Vec<Value>) -> Value {
    mouse_action(args, core::mclick)
}

pub fn lpress(args: Vec<Value>) -> Value {
    mouse_action(args, core::lpress)
}
pub fn rpress(args: Vec<Value>) -> Value {
    mouse_action(args, core::rpress)
}
pub fn mpress(args: Vec<Value>) -> Value {
    mouse_action(args, core::mpress)
}

pub fn lrelease(args: Vec<Value>) -> Value {
    mouse_action(args, core::lrelease)
}
pub fn rrelease(args: Vec<Value>) -> Value {
    mouse_action(args, core::rrelease)
}
pub fn mrelease(args: Vec<Value>) -> Value {
    mouse_action(args, core::mrelease)
}

pub fn sleep(args: Vec<Value>) -> Value {
    if let Some(Value::Duration(dur)) = args.get(0) {
        std::thread::sleep(*dur);
    }
    Value::Null
}

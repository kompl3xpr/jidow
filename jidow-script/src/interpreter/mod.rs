mod env;
use env::Env;

mod expr;
mod func;
mod stmt;
use stmt::ControlFlow;

use crate::{locate::Located, parser::ast::Expr};

use self::{error::RuntimeError, func::Function};
pub mod error;

pub type RuntimeResult<T> = Result<T, RuntimeError>;

pub struct Interpreter<'ast> {
    env: Env<'ast>,
    control: Option<ControlFlow>,
    in_func: Option<Function<'ast>>,
    ret: Option<&'ast Located<Expr>>,
}

impl<'ast> Interpreter<'ast> {
    pub fn new() -> Self {
        Self {
            env: Env::new(),
            control: None,
            ret: None,
            in_func: None,
        }
    }

    pub fn in_func(f: Function<'ast>) -> Self {
        Self {
            in_func: Some(f),
            env: Env::new(),
            control: None,
            ret: None,
        }
    }
}

use super::{
    env::{Scope, GLOBAL},
    error::ErrorInfo::*,
    expr::Value,
    Interpreter, RuntimeResult,
};
use crate::{ast::Stmt, locate::Located};

#[derive(Debug, Clone, Copy)]
pub(super) enum ControlFlow {
    Return,
    Break,
    Continue,
}

impl<'a> Interpreter<'a> {
    pub fn exec(&mut self, stmt: &'a Located<Stmt>) -> RuntimeResult<()> {
        match stmt.as_inner() {
            Stmt::Block(stmts) => {
                // RAII is used here, which means that
                // variables in the scope will be removed when `scope` drops.
                let mut scope = Scope::new(self);
                for stmt in stmts {
                    scope.exec(stmt)?;
                    if let Some(_) = scope.control {
                        return Ok(());
                    }
                }
            }
            Stmt::LetBe(var, expr) => {
                let value = self.eval(expr)?;
                if GLOBAL.contains_key(var.as_inner().as_str()) {
                    return Err(var.here(ShadowGlobalVar(var.as_inner().clone())))?;
                }
                self.env.insert(var.as_inner(), value);
            }
            Stmt::ExprStmt(expr) => match self.eval(expr)? {
                Value::BuiltinFunc(f) => {
                    f.call(vec![]);
                }
                Value::Func(f) => {
                    f.call(vec![])?;
                }
                _ => {}
            },
            Stmt::None => {}

            Stmt::SetTo(var, expr) => {
                let value = self.eval(expr)?;
                match self.env.get_mut(var.as_inner()) {
                    Ok(Some(val)) => *val = value,
                    Ok(None) => return Err(var.here(VariableNotFound(var.as_inner().clone())))?,
                    Err(()) => return Err(var.here(WriteGlobalVar(var.as_inner().clone())))?,
                }
            }

            Stmt::Condition { arms, other } => {
                for (expr, body) in arms {
                    match self.eval(expr)? {
                        Value::Boolean(true) => return self.exec(body),
                        Value::Boolean(_) => continue,
                        x => return Err(expr.here(TypeError("Boolean", x.type_name())))?,
                    }
                }
                if let Some(other) = other {
                    self.exec(other)?;
                }
            }

            Stmt::ForLoop { item, array, body } => {
                let values: Box<dyn Iterator<Item = Value>> = match self.eval(array)? {
                    Value::Array(x) => Box::new(x.into_iter()),
                    Value::Range(begin, end, step) => {
                        let iter = (begin..end).map(|x| Value::Integer(x));
                        match step {
                            Some(s) if s > 0 => Box::new(iter.step_by(s as usize)),
                            Some(neg) => Box::new(iter.rev().step_by(-neg as usize)),
                            None => Box::new(iter),
                        }
                    }
                    other => return Err(array.here(TypeError("Array", other.type_name())))?,
                };
                for val in values {
                    let mut scope = Scope::new(self);
                    scope.env.insert(item.as_inner(), val);
                    scope.exec(body)?;
                    if let Some(control) = scope.control {
                        match control {
                            ControlFlow::Continue => {
                                scope.control = None;
                                continue;
                            }
                            ControlFlow::Break => {
                                scope.control = None;
                                break;
                            }
                            ControlFlow::Return => break,
                        }
                    }
                }
            }

            Stmt::WhileLoop { cond, body } => loop {
                match self.eval(cond)? {
                    Value::Boolean(true) => (),
                    Value::Boolean(false) => break,
                    other => return Err(cond.here(TypeError("Boolean", other.type_name())))?,
                }
                self.exec(body)?;
                if let Some(control) = self.control {
                    match control {
                        ControlFlow::Continue => {
                            self.control = None;
                            continue;
                        }
                        ControlFlow::Break => {
                            self.control = None;
                            break;
                        }
                        ControlFlow::Return => break,
                    }
                }
            },

            Stmt::Break => self.control = Some(ControlFlow::Break),
            Stmt::Continue => self.control = Some(ControlFlow::Continue),
            Stmt::Return(expr) => {
                self.control = Some(ControlFlow::Return);
                self.ret = Some(expr);
            }
        }
        Ok(())
    }
}

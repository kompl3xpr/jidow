use crate::locate::Located;
use std::fmt::{Debug, Display};

#[derive(Clone)]
pub struct RuntimeError(Located<ErrorInfo>);

#[derive(Debug, Clone)]
pub enum ErrorInfo {
    /// `(Expected, Found)`
    TypeError(&'static str, &'static str),
    VariableNotFound(String),
    FailedToAccess(String),
    WriteGlobalVar(String),
    ShadowGlobalVar(String),
    IndexOutOfBounds(i32),
    InvalidUseOfSelf,
}

impl Debug for RuntimeError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        self.0.fmt(f)
    }
}

impl Display for RuntimeError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{:?}", self)
    }
}

impl std::error::Error for RuntimeError {}

impl From<Located<ErrorInfo>> for RuntimeError {
    fn from(e: Located<ErrorInfo>) -> Self {
        Self(e)
    }
}

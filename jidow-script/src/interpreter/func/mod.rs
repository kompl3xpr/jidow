use crate::{
    locate::Located,
    parser::ast::{Expr, Stmt},
};

use super::{
    env::{Env, Scope},
    error::{ErrorInfo::*, RuntimeError},
    expr::Value,
    Interpreter,
};

mod capture;

#[derive(Clone)]
pub struct Function<'ast> {
    params: Vec<&'ast str>,
    body: &'ast Located<Stmt>,
    pub env: Env<'ast>,
}

impl<'a> Function<'a> {
    pub(super) fn try_new(
        params: Vec<&'a str>,
        body: &'a Located<Stmt>,
        _itp: &Interpreter,
    ) -> Result<Self, RuntimeError> {
        let env = Env::new();
        let res = Self { params, body, env };
        // todo: variable capturing
        Ok(res)
    }
}

impl<'a> Function<'a> {
    pub(super) fn call(self, args: Vec<Value<'a>>) -> Result<Value<'a>, RuntimeError> {
        let mut itp = Interpreter::in_func(self);
        let mut arg_list = Some(args);

        // Tail Call Optimization is implemented here
        loop {
            let mut scope = Scope::new(&mut itp);
            let expr = match scope.apply(arg_list.take().unwrap())? {
                None => return Ok(Value::Null),
                Some(expr) => expr,
            };
            let (f, args) = match expr.as_inner() {
                Expr::Call(f, args) => (f, args),
                _ => return scope.eval(expr),
            };
            match f.as_inner() {
                Expr::FnSelf => {
                    let args = args.iter().map(|x| scope.eval(x));
                    arg_list = Some(args.collect::<Result<_, _>>()?)
                }
                _ => match scope.eval(f)? {
                    Value::Func(func) => {
                        let args = args.iter().map(|x| scope.eval(x));
                        arg_list = Some(args.collect::<Result<_, _>>()?);
                        drop(scope);
                        itp = Interpreter::in_func(func);
                    }
                    Value::BuiltinFunc(_) => return scope.eval(expr),
                    other => Err(f.here(TypeError("Function", other.type_name())))?,
                },
            }
        }
    }
}

impl<'ast> Interpreter<'ast> {
    fn apply(
        &mut self,
        args: Vec<Value<'ast>>,
    ) -> Result<Option<&'ast Located<Expr>>, RuntimeError> {
        let args = self.in_func.as_ref().unwrap().params.iter().zip(args);
        args.for_each(|(symbol, val)| self.env.insert(symbol, val));
        let stmts = match self.in_func.as_ref().unwrap().body.as_inner() {
            Stmt::Block(stmts) => stmts,
            Stmt::Return(expr) => return Ok(Some(expr)),
            _ => unreachable!(),
        };
        for stmt in stmts {
            self.exec(stmt)?;
            if let Some(_) = self.control {
                return Ok(self.ret);
            }
        }
        Ok(self.ret)
    }
}

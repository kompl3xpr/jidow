use super::{error::ErrorInfo::*, func::Function, Interpreter, RuntimeResult};
use crate::{
    ast::Expr::{self, Add, And, Div, Eq, Geq, Gt, Leq, Lt, Mul, Or, Pow, Rem, Sub},
    locate::Located,
};
use jidow_core::{Color, KeyCode};
use std::{fmt::Debug, time::Duration};

#[derive(Clone)]
pub enum Value<'a> {
    Null,
    Range(i32, i32, Option<i32>),
    Array(Vec<Value<'a>>),
    Color(Color),
    Duration(Duration),
    Boolean(bool),
    Integer(i32),
    Float(f64),
    Str(String),
    KeyCode(KeyCode),
    BuiltinFunc(&'static dyn Func),
    Object(&'static dyn Object),
    Func(Function<'a>),
}

impl<'a> Value<'a> {
    pub fn type_name(&self) -> &'static str {
        match self {
            Value::Null => "Null",
            Value::Array(_) => "Array",
            Value::Color(_) => "Color",
            Value::Duration(_) => "Duration",
            Value::Boolean(_) => "Boolean",
            Value::Integer(_) => "Integer",
            Value::Float(_) => "Float",
            Value::Str(_) => "String",
            Value::KeyCode(_) => "Key Code",
            Value::BuiltinFunc(_) => "Function",
            Value::Object(_) => "Object",
            Value::Range(_, _, _) => "Range",
            Value::Func(_) => "Function",
        }
    }
}

impl<'a> Debug for Value<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Value::Null => write!(f, "null"),
            Value::Array(vals) => f.debug_list().entries(vals).finish(),
            Value::Color(color) => write!(f, "{:?}", color),
            Value::Duration(dur) => write!(f, "{:?}", dur),
            Value::Boolean(b) => write!(f, "{}", b),
            Value::Integer(n) => write!(f, "{}", n),
            Value::Float(n) => write!(f, "{:.4}", n),
            Value::Str(s) => write!(f, "{}", s),
            Value::KeyCode(k) => write!(f, "{:?}", k),
            Value::BuiltinFunc(_) => write!(f, "<Function>"),
            Value::Func(_) => write!(f, "<Function>"),
            Value::Object(_) => write!(f, "<Object>"),
            Value::Range(_, _, _) => write!(f, "<Range>"),
        }
    }
}

pub trait Func: Send + Sync {
    fn call<'a>(&self, args: Vec<Value<'a>>) -> Value<'a>;
}

pub trait Object: Send + Sync {
    fn access(&self, member: &str) -> Option<Value>;
}

impl<'a> Interpreter<'a> {
    pub fn eval(&mut self, expr: &'a Located<Expr>) -> RuntimeResult<Value<'a>> {
        match expr.as_inner() {
            Expr::HexColor(x) => Ok(Value::Color(Color::rgb_from_u32(*x))),
            Expr::Duration(x) => Ok(Value::Duration(*x)),
            Expr::LitInt(x) => Ok(Value::Integer(*x as i32)),
            Expr::LitFloat(x) => Ok(Value::Float(*x)),
            Expr::LitStr(x) => Ok(Value::Str(x.clone())),
            Expr::True => Ok(Value::Boolean(true)),
            Expr::False => Ok(Value::Boolean(false)),
            Expr::LitArr(xs) => Ok(Value::Array(
                xs.iter()
                    .map(|x| self.eval(x))
                    .collect::<Result<Vec<_>, _>>()?,
            )),
            Expr::Neg(x) => match self.eval(x)? {
                Value::Integer(i) => Ok(Value::Integer(-i)),
                Value::Float(f) => Ok(Value::Float(-f)),
                other => Err(x.here(TypeError("Numeric", other.type_name())))?,
            },

            Pow(x, y) | Mul(x, y) | Div(x, y) | Add(x, y) | Sub(x, y) | Rem(x, y) => {
                type IntFn = &'static dyn Fn(i32, i32) -> i32;
                type FltFn = &'static dyn Fn(f64, f64) -> f64;
                let (int_fn, flt_fn): (IntFn, FltFn) = match expr.as_inner() {
                    Pow(_, _) => (&|x, y| x.pow(y as u32), &f64::powf),
                    Mul(_, _) => (&|x, y| x * y, &|x, y| x * y),
                    Div(_, _) => (&|x, y| x / y, &|x, y| x / y),
                    Add(_, _) => (&|x, y| x + y, &|x, y| x + y),
                    Sub(_, _) => (&|x, y| x - y, &|x, y| x - y),
                    Rem(_, _) => (&|x, y| x % y, &|x, y| x % y),
                    _ => unreachable!(),
                };
                match (self.eval(x)?, self.eval(y)?) {
                    (Value::Integer(x), Value::Integer(y)) => Ok(Value::Integer(int_fn(x, y))),
                    (Value::Integer(x), Value::Float(y)) => Ok(Value::Float(flt_fn(x as f64, y))),
                    (Value::Float(x), Value::Integer(y)) => Ok(Value::Float(flt_fn(x, y as f64))),
                    (Value::Float(x), Value::Float(y)) => Ok(Value::Float(flt_fn(x, y))),
                    (a, Value::Integer(_) | Value::Float(_)) => {
                        Err(x.here(TypeError("Number", a.type_name())))?
                    }
                    (_, b) => Err(y.here(TypeError("Number", b.type_name())))?,
                }
            }

            Expr::Not(x) => match self.eval(x)? {
                Value::Boolean(x) => Ok(Value::Boolean(!x)),
                other => Err(x.here(TypeError("Boolean", other.type_name())))?,
            },

            And(x, y) | Or(x, y) => {
                let op: &'static dyn Fn(bool, bool) -> bool = match expr.as_inner() {
                    And(_, _) => &|x, y| x && y,
                    Or(_, _) => &|x, y| x || y,
                    _ => unreachable!(),
                };
                match (self.eval(x)?, self.eval(y)?) {
                    (Value::Boolean(x), Value::Boolean(y)) => Ok(Value::Boolean(op(x, y))),
                    (a, Value::Boolean(_)) => Err(x.here(TypeError("Boolean", a.type_name())))?,
                    (_, b) => Err(y.here(TypeError("Boolean", b.type_name())))?,
                }
            }

            Lt(x, y) | Gt(x, y) | Eq(x, y) | Leq(x, y) | Geq(x, y) => {
                type IntFn = &'static dyn Fn(i32, i32) -> bool;
                type FltFn = &'static dyn Fn(f64, f64) -> bool;
                let (int_fn, flt_fn): (IntFn, FltFn) = match expr.as_inner() {
                    Lt(_, _) => (&|x, y| x < y, &|x, y| x < y),
                    Gt(_, _) => (&|x, y| x > y, &|x, y| x > y),
                    Leq(_, _) => (&|x, y| x <= y, &|x, y| x <= y),
                    Geq(_, _) => (&|x, y| x >= y, &|x, y| x >= y),
                    Eq(_, _) => (&|x, y| x == y, &|x, y| x == y),
                    _ => unreachable!(),
                };
                match (self.eval(x)?, self.eval(y)?) {
                    (Value::Integer(x), Value::Integer(y)) => Ok(Value::Boolean(int_fn(x, y))),
                    (Value::Integer(x), Value::Float(y)) => Ok(Value::Boolean(flt_fn(x as f64, y))),
                    (Value::Float(x), Value::Integer(y)) => Ok(Value::Boolean(flt_fn(x, y as f64))),
                    (Value::Float(x), Value::Float(y)) => Ok(Value::Boolean(flt_fn(x, y))),
                    (a, Value::Integer(_) | Value::Float(_)) => {
                        Err(x.here(TypeError("Number", a.type_name())))?
                    }
                    (_, b) => Err(y.here(TypeError("Number", b.type_name())))?,
                }
            }

            Expr::Var(var) => {
                // to be optimized...
                let value = self.env.get(var).cloned();
                value.ok_or(expr.here(VariableNotFound(var.into())).into())
            }

            Expr::Access(obj, member) => match self.eval(obj)? {
                Value::Object(obj) => obj.access(member.as_inner()).ok_or_else(|| {
                    member
                        .here(FailedToAccess(member.as_inner().clone()))
                        .into()
                }),
                other => Err(obj.here(TypeError("Object", other.type_name())))?,
            },

            Expr::Index(arr, index) => match (self.eval(arr)?, self.eval(index)?) {
                (Value::Array(arr), Value::Integer(idx)) => arr
                    .get(idx as usize)
                    .cloned()
                    .ok_or_else(|| index.here(IndexOutOfBounds(idx)).into()),
                (x, Value::Integer(_)) => Err(arr.here(TypeError("Array", x.type_name())))?,
                (_, x) => Err(index.here(TypeError("Integer", x.type_name())))?,
            },

            Expr::Call(func, args) => {
                let args = args
                    .into_iter()
                    .map(|a| self.eval(a))
                    .collect::<Result<Vec<_>, _>>()?;
                match self.eval(func)? {
                    Value::BuiltinFunc(func) => Ok(func.call(args)),
                    Value::Func(func) => func.call(args),
                    other => Err(func.here(TypeError("Function", other.type_name())))?,
                }
            }

            Expr::Func { params, body } => Ok(Value::Func(Function::try_new(
                params.into_iter().map(|x| x.as_inner().as_ref()).collect(),
                body,
                &self,
            )?)),

            Expr::FnSelf => match &self.in_func {
                Some(func) => Ok(Value::Func(func.clone())),
                None => Err(expr.here(InvalidUseOfSelf))?,
            },
        }
    }
}

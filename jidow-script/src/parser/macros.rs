macro_rules! grammar {
    ($name: ident : $ty: ty => let [$($cmb: tt)*] in [$($args: ident)*] return $out: block) => {
        #[allow(unused)]
        pub fn $name<'t>(
            tokens: $crate::parser::Tokens<'t>,
            ctx: &mut $crate::parser::Context,
        ) -> $crate::parser::MaybeParsed<'t, $ty> {
            let mut p = combine!([] $($cmb)*);
            p.map(|nested_tuple![[] $($args)*]| $out).parse(tokens, ctx)
        }
    };
}

macro_rules! nested_tuple {
    ([$($out:tt)*]) => {
        $($out)*
    };
    ([] $x: tt $($xs: ident)*) => {
        nested_tuple!([mut $x] $($xs)*)
    };
    ([$($out:tt)*] $x: ident $($xs: tt)*) => {
        nested_tuple!([($($out)*, mut $x)] $($xs)*)
    }
}

macro_rules! combine {
    ([$($out:tt)*]) => {
        $($out)*
    };

    ([] @$x:ident $($other:tt)*) => {
        combine!([$crate::parser::base::KeywordP($x).c()] $($other)*)
    };
    ([] .$x:ident $($other:tt)*) => {
        combine!([$crate::parser::base::PunctP($x).c()] $($other)*)
    };
    ([] [$x:expr] $($other:tt)*) => {
        combine!([$crate::parser::base::Just($x).c()] $($other)*)
    };
    ([] $x:ident $($other:tt)*) => {
        combine!([$x.c()] $($other)*)
    };

    ([$($out:tt)*] @$x:ident $($other:tt)*) => {
        combine!([$($out)* $crate::parser::base::KeywordP($x).c()] $($other)*)
    };
    ([$($out:tt)*] .$x:ident $($other:tt)*) => {
        combine!([$($out)* $crate::parser::base::PunctP($x).c()] $($other)*)
    };
    ([$($out:tt)*] [$x:expr] $($other:tt)*) => {
        combine!([$($out)* $crate::parser::base::Just($x).c()] $($other)*)
    };
    ([$($out:tt)*] $x:ident $($other:tt)*) => {
        combine!([$($out)* $x.c()] $($other)*)
    };

    ([$($out:tt)*] >> $($other:tt)*) => {
        combine!([$($out)* ^] $($other)*)
    };
    ([$($out:tt)*] & $($other:tt)*) => {
        combine!([$($out)* &] $($other)*)
    };
    ([$($out:tt)*] | $($other:tt)*) => {
        combine!([$($out)* |] $($other)*)
    };
    ([$($out:tt)*] << $($other:tt)*) => {
        combine!([$($out)*.c() <<] $($other)*)
    };
    ([$($out:tt)*] <m> $f:ident $($other:tt)*) => {
        combine!([$($out)*.map($f)] $($other)*)
    };
    ([$($out:tt)*] <m> |$($args: ident)*| $body:block $($other:tt)*) => {
        combine!([$($out)*.map(|nested_tuple![[] $($args)*]| $body)] $($other)*)
    };
    ([$($out:tt)*] ($($block: tt)*) $($other:tt)*) => {{
        let inner = combine!([] $($block)*);
        combine!([$($out)* inner] $($other)*)
    }};
}

pub(super) use {combine, grammar, nested_tuple};

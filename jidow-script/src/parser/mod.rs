mod base;
mod context;
mod expr;
mod macros;
mod stmt;

pub mod ast;
pub mod error;
pub use self::context::Context;
pub use {expr::expr as ExpressionParser, stmt::stmt as StatmentParser};

use crate::{tokenizer::tokens::Token, Located};
use error::ParseError;
use std::ops::{BitAnd, BitOr, BitXor, Shl};

pub(super) type MaybeParsed<'a, T> = Option<(T, Tokens<'a>)>;
pub(super) type Tokens<'a> = &'a [Located<Token>];

pub trait Parse: Sized {
    type Output;
    fn parse<'a>(&mut self, tokens: Tokens<'a>, ctx: &mut Context)
        -> MaybeParsed<'a, Self::Output>;
    fn map<U, F: FnMut(Self::Output) -> U>(self, f: F) -> Combinable<Map<Self, F>> {
        Map(self, f).c()
    }

    fn c(self) -> Combinable<Self> {
        Combinable(self)
    }
}

pub struct Or<P: Parse, Q: Parse>(P, Q);
impl<P, Q, O> Parse for Or<P, Q>
where
    P: Parse<Output = O>,
    Q: Parse<Output = O>,
{
    type Output = O;
    #[inline(always)]
    fn parse<'a>(
        &mut self,
        tokens: Tokens<'a>,
        ctx: &mut Context,
    ) -> MaybeParsed<'a, Self::Output> {
        match self.0.parse(tokens, ctx) {
            Some(result) => Some(result),
            None => match self.1.parse(tokens, ctx) {
                Some(result) => Some(result),
                None => None,
            },
        }
    }
}

pub struct And<P: Parse, Q: Parse>(P, Q);
impl<P: Parse, Q: Parse> Parse for And<P, Q> {
    type Output = (P::Output, Q::Output);
    #[inline(always)]
    fn parse<'a>(
        &mut self,
        tokens: Tokens<'a>,
        ctx: &mut Context,
    ) -> MaybeParsed<'a, Self::Output> {
        let (out1, rest1) = self.0.parse(tokens, ctx)?;
        let (out2, rest2) = self.1.parse(rest1, ctx)?;
        Some(((out1, out2), rest2))
    }
}

pub struct Then<P: Parse, Q: Parse>(P, Q);
impl<P: Parse, Q: Parse> Parse for Then<P, Q> {
    type Output = Q::Output;
    #[inline(always)]
    fn parse<'a>(
        &mut self,
        tokens: Tokens<'a>,
        ctx: &mut Context,
    ) -> MaybeParsed<'a, Self::Output> {
        let (_, rest1) = self.0.parse(tokens, ctx)?;
        let (out2, rest2) = self.1.parse(rest1, ctx)?;
        Some((out2, rest2))
    }
}

pub struct With<P: Parse, Q: Parse>(P, Q);
impl<P: Parse, Q: Parse> Parse for With<P, Q> {
    type Output = P::Output;
    #[inline(always)]
    fn parse<'a>(
        &mut self,
        tokens: Tokens<'a>,
        ctx: &mut Context,
    ) -> MaybeParsed<'a, Self::Output> {
        let (out1, rest1) = self.0.parse(tokens, ctx)?;
        let (_, rest2) = self.1.parse(rest1, ctx)?;
        Some((out1, rest2))
    }
}

pub struct Map<P: Parse, F>(P, F);
impl<P: Parse, F, U> Parse for Map<P, F>
where
    F: FnMut(P::Output) -> U,
{
    type Output = U;
    #[inline(always)]
    fn parse<'a>(
        &mut self,
        tokens: Tokens<'a>,
        ctx: &mut Context,
    ) -> MaybeParsed<'a, Self::Output> {
        self.0
            .parse(tokens, ctx)
            .map(|(out, tokens)| ((self.1)(out), tokens))
    }
}

pub struct Combinable<P: Parse>(P);

impl<P: Parse> Parse for Combinable<P> {
    type Output = P::Output;
    #[inline(always)]
    fn parse<'a>(
        &mut self,
        tokens: Tokens<'a>,
        ctx: &mut Context,
    ) -> MaybeParsed<'a, Self::Output> {
        self.0.parse(tokens, ctx)
    }
}

impl<P, Q, O> BitOr<Q> for Combinable<P>
where
    P: Parse<Output = O>,
    Q: Parse<Output = O>,
{
    type Output = Combinable<Or<P, Q>>;
    #[inline(always)]
    fn bitor(self, rhs: Q) -> Self::Output {
        Or(self.0, rhs).c()
    }
}

impl<P: Parse, Q: Parse> BitAnd<Q> for Combinable<P> {
    type Output = Combinable<And<P, Q>>;
    #[inline(always)]
    fn bitand(self, rhs: Q) -> Self::Output {
        And(self.0, rhs).c()
    }
}

impl<P: Parse, Q: Parse> BitXor<Q> for Combinable<P> {
    type Output = Combinable<Then<P, Q>>;
    #[inline(always)]
    fn bitxor(self, rhs: Q) -> Self::Output {
        Then(self.0, rhs).c()
    }
}

impl<P: Parse, Q: Parse> Shl<Q> for Combinable<P> {
    type Output = Combinable<With<P, Q>>;
    #[inline(always)]
    fn shl(self, rhs: Q) -> Self::Output {
        With(self.0, rhs).c()
    }
}

impl<O, F> Parse for F
where
    F: for<'t> FnMut(Tokens<'t>, &mut Context) -> MaybeParsed<'t, O>,
{
    type Output = O;

    #[inline(always)]
    fn parse<'a>(&mut self, tokens: Tokens<'a>, ctx: &mut Context) -> MaybeParsed<'a, O> {
        self(tokens, ctx)
    }
}

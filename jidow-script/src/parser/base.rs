use super::{ast::Expr, *};
use crate::{tokenizer::tokens::*, Located};

struct Is<F: Fn(&Token) -> bool>(pub F);
impl<F: Fn(&Token) -> bool> Parse for Is<F> {
    type Output = Located<()>;
    fn parse<'a>(
        &mut self,
        tokens: Tokens<'a>,
        ctx: &mut Context,
    ) -> MaybeParsed<'a, Self::Output> {
        if let Some(token) = tokens.get(0) {
            if self.0(token.as_inner()) {
                return Some((token.as_ref().map(|_| ()), &tokens[1..]));
            }
            ctx.update_error(ParseError::UnexpectedToken(token.clone()));
            return None;
        }
        ctx.update_error(ParseError::EndWhileParsing);
        None
    }
}

struct Try<O, F: Fn(&Token) -> Option<O>>(pub F);
impl<O, F: Fn(&Token) -> Option<O>> Parse for Try<O, F> {
    type Output = Located<O>;
    fn parse<'a>(
        &mut self,
        tokens: Tokens<'a>,
        ctx: &mut Context,
    ) -> MaybeParsed<'a, Self::Output> {
        if let Some(token) = tokens.get(0) {
            if let Some(res) = self.0(token.as_inner()) {
                return Some((token.as_ref().here(res), &tokens[1..]));
            }
            ctx.update_error(ParseError::UnexpectedToken(token.clone()));
            return None;
        }
        ctx.update_error(ParseError::EndWhileParsing);
        None
    }
}

pub struct Just<T>(pub T);
impl<T: Clone> Parse for Just<T> {
    type Output = T;
    fn parse<'a>(&mut self, tokens: Tokens<'a>, _: &mut Context) -> MaybeParsed<'a, T> {
        Some((self.0.clone(), tokens))
    }
}

pub struct KeywordP(pub Keyword);
impl Parse for KeywordP {
    type Output = Located<Keyword>;
    fn parse<'a>(
        &mut self,
        tokens: Tokens<'a>,
        ctx: &mut Context,
    ) -> MaybeParsed<'a, Located<Keyword>> {
        let pred = |tok: &Token| match tok {
            Token::Keyword(k) if *k == self.0 => true,
            _ => false,
        };
        Is(pred).map(|x| x.map(|_| self.0)).parse(tokens, ctx)
    }
}

pub struct PunctP(pub Punct);
impl Parse for PunctP {
    type Output = Located<Punct>;
    fn parse<'a>(
        &mut self,
        tokens: Tokens<'a>,
        ctx: &mut Context,
    ) -> MaybeParsed<'a, Located<Punct>> {
        let pred = |tok: &Token| match tok {
            Token::Punct(p) if *p == self.0 => true,
            _ => false,
        };
        Is(pred).map(|x| x.map(|_| self.0)).parse(tokens, ctx)
    }
}

pub fn ident<'t>(tokens: Tokens<'t>, ctx: &mut Context) -> MaybeParsed<'t, Located<String>> {
    let f = |tok: &Token| match tok {
        Token::Ident(s) => Some(s.to_owned()),
        _ => None,
    };
    Try(f).parse(tokens, ctx)
}

pub fn string<'t>(tokens: Tokens<'t>, ctx: &mut Context) -> MaybeParsed<'t, Located<Expr>> {
    let f = |tok: &Token| match tok {
        Token::Str(s) => Some(Expr::LitStr(s.to_owned())),
        _ => None,
    };
    Try(f).parse(tokens, ctx)
}

pub fn scope_begin<'t>(tokens: Tokens<'t>, ctx: &mut Context) -> MaybeParsed<'t, Located<()>> {
    Is(|x: &Token| matches!(x, Token::ScopeBegin)).parse(tokens, ctx)
}

pub fn scope_end<'t>(tokens: Tokens<'t>, ctx: &mut Context) -> MaybeParsed<'t, Located<()>> {
    Is(|x: &Token| matches!(x, Token::ScopeEnd)).parse(tokens, ctx)
}

pub fn next_ln<'t>(tokens: Tokens<'t>, ctx: &mut Context) -> MaybeParsed<'t, Located<()>> {
    Is(|x: &Token| matches!(x, Token::NextLn)).parse(tokens, ctx)
}

pub fn var<'t>(tokens: Tokens<'t>, ctx: &mut Context) -> MaybeParsed<'t, Located<Expr>> {
    ident.map(|s| s.map(|s| Expr::Var(s))).parse(tokens, ctx)
}

pub fn int<'t>(tokens: Tokens<'t>, ctx: &mut Context) -> MaybeParsed<'t, Located<Expr>> {
    let f = |tok: &Token| match tok {
        Token::Integer(n) => Some(Expr::LitInt(*n)),
        _ => None,
    };
    Try(f).parse(tokens, ctx)
}

pub fn float<'t>(tokens: Tokens<'t>, ctx: &mut Context) -> MaybeParsed<'t, Located<Expr>> {
    let f = |tok: &Token| match tok {
        Token::Float(f) => Some(Expr::LitFloat(*f)),
        _ => None,
    };
    Try(f).parse(tokens, ctx)
}

pub fn hex_color<'t>(tokens: Tokens<'t>, ctx: &mut Context) -> MaybeParsed<'t, Located<Expr>> {
    let f = |tok: &Token| match tok {
        Token::HexColor(c) => Some(Expr::HexColor(*c)),
        _ => None,
    };
    Try(f).parse(tokens, ctx)
}

pub fn duration<'t>(tokens: Tokens<'t>, ctx: &mut Context) -> MaybeParsed<'t, Located<Expr>> {
    let f = |tok: &Token| match tok {
        Token::Duration(d) => Some(Expr::Duration(*d)),
        _ => None,
    };
    Try(f).parse(tokens, ctx)
}

pub fn collect<T>((x, mut xs): (T, Vec<T>)) -> Vec<T> {
    xs.push(x);
    xs
}

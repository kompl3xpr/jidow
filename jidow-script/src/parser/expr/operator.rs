use crate::{
    locate::Located,
    parser::{macros::*, Parse},
    tokens::{Keyword::*, Punct::*},
};

#[derive(Clone)]
#[rustfmt::skip]
pub enum Oprt {
    And, Or, Not, 
    Mul, Div, Rem,
    Add, Sub, Neg,
    Lt, Gt, Leq, Geq, Eq,
}

grammar! {add_sub_op: Oprt =>
    let [[Oprt::Add] <<.Plus | [Oprt::Sub] <<.Minus] in [op] return { op }
}
grammar! {mul_div_rem_op: Oprt =>
    let [[Oprt::Mul] <<.Asterisk | [Oprt::Div] <<.Slash | [Oprt::Rem] <<.Percent] in [op]
    return { op }
}
grammar! {neg_not_op: Located<Oprt> =>
    let [.DoubleMinus <m> |x| { x.here(Oprt::Neg) }
    | (@Not <m> |x| { x.here(Oprt::Not) })] in [op]
    return{ op }
}
grammar! {log_op: Oprt =>
    let [[Oprt::And] <<@And | [Oprt::Or] <<@Or] in [op]
    return { op }
}
grammar! {cmp_op: Oprt =>
    let [[Oprt::Lt] <<.Lt | [Oprt::Gt] <<.Gt | [Oprt::Eq] <<.Eq
        | [Oprt::Leq] <<.Leq | [Oprt::Geq] <<.Geq] in [op]
    return { op }
}

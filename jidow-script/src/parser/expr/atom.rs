use super::expr;
use crate::{
    locate::Located,
    parser::{
        ast::{Expr, Stmt},
        base::*,
        macros::*,
        stmt::expr_block,
        Parse,
    },
    tokens::{Keyword::*, Punct::*},
};

grammar! {true_: Located<Expr> => let [@True] in [x] return { x.here(Expr::True) }}
grammar! {false_: Located<Expr> => let [@False] in [x] return { x.here(Expr::False) }}

grammar! {not_var: Located<Expr> =>
    let [true_ | false_ | duration | hex_color | func | self_
    | int | float | string | arr | (.LParen>> expr <<.RParen)] in [x]
    return { x }
}

grammar! {atom: Located<Expr> =>
    let [not_var | var] in [x]
    return { x }
}

grammar! {arr: Located<Expr> =>
    let [.LBrace & maybe_elems <<.RBrace] in [loc items]
    return {
        items.reverse();
        loc.here(Expr::LitArr(items))
    }
}
grammar! {maybe_elems: Vec<Located<Expr>> =>
    let [elems | [vec![]]] in [x] return { x }
}
grammar! {elems: Vec<Located<Expr>>  =>
    let [expr & _elems] in [x xs] return { collect((x, xs)) }
}
grammar! {_elems: Vec<Located<Expr>> =>
    let [.Comma>> elems | [vec![]]] in [x] return { x }
}

grammar! {params: Vec<Located<String>> =>
    let [(ident & params) <m> collect | [vec![]]] in [x] return { x }
}

grammar! {func: Located<Expr> =>
    let [@Func & params & (@That>> (expr <m> |x| {
        let loc = x.here(());
        loc.here(Stmt::Return(Box::new(x)))
    } | next_ln>> expr_block))]
    in [loc ps body]
    return {
        ps.reverse();
        loc.here(Expr::Func {
            params: ps,
            body: Box::new(body),
        })
    }
}

grammar! {self_: Located<Expr> => let [@Self_] in [x] return { x.here(Expr::FnSelf) }}

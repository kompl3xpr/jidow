use super::{ast::Expr, base::*, macros::*, Parse};
use crate::locate::Located;
use crate::tokenizer::tokens::Punct::*;

mod operator;
pub use operator::*;
mod atom;
pub use atom::*;

grammar! {expr: Located<Expr> => let [cmp & may_call] in [x y] return {
    let loc = x.here(());
    match y {
        Some(args) => loc.here(Expr::Call(Box::new(x), args)),
        None => x,
    }
}}
grammar! {may_call: Option<Vec<Located<Expr>>> =>
    let [(.Colon>> args) <m> |x| { x.reverse(); Some(x) }
        | [None]] in [x]
    return { x }
}
grammar! {args: Vec<Located<Expr>> => let [(expr & args) <m> collect | [vec![]]] in [x] return { x }}

grammar! {access: Located<Expr> => let [atom & _access] in [x y] return {
    let loc = x.here(());
    y.into_iter().rev().fold(x, |i, e| loc.here(Expr::Access(Box::new(i), e)))
}}
grammar! {index: Located<Expr> => let [access & _index] in [x y] return {
    let loc = x.here(());
    y.into_iter().rev().fold(x, |i, e| loc.here(Expr::Index(Box::new(i), Box::new(e))))
}}
grammar! {neg_not: Located<Expr> => let [_neg_not | index] in [x] return { x }}

grammar! {pow: Located<Expr> => let [neg_not & _pow] in [x y] return {
    let loc = x.here(());
    y.into_iter().rev().fold(x, |i, e| loc.here(Expr::Pow(Box::new(i), Box::new(e))))
}}
grammar! {mul_div_rem: Located<Expr> => let [pow & _mul_div_rem] in [x y] return {
    let loc = x.here(());
    y.into_iter().rev().fold(x, |i, e| {
        match &e.0 {
            Oprt::Mul => loc.here(Expr::Mul(Box::new(i), Box::new(e.1))),
            Oprt::Div => loc.here(Expr::Div(Box::new(i), Box::new(e.1))),
            Oprt::Rem => loc.here(Expr::Rem(Box::new(i), Box::new(e.1))),
            _ => unreachable!(),
        }
    })
}}
grammar! {add_sub:Located<Expr> => let [mul_div_rem & _add_sub] in [x y] return {
    let loc = x.here(());
    y.into_iter().rev().fold(x, |i, e| {
        match &e.0 {
            Oprt::Add => loc.here(Expr::Add(Box::new(i), Box::new(e.1))),
            Oprt::Sub => loc.here(Expr::Sub(Box::new(i), Box::new(e.1))),
            _ => unreachable!(),
        }
    })
}}
grammar! {logical: Located<Expr> => let [add_sub & _logical] in [x y] return {
    let loc = x.here(());
    y.into_iter().rev().fold(x, |i, e| {
        match &e.0 {
            Oprt::And => loc.here(Expr::And(Box::new(i), Box::new(e.1))),
            Oprt::Or => loc.here(Expr::Or(Box::new(i), Box::new(e.1))),
            _ => unreachable!(),
        }
    })
}}
grammar! {cmp: Located<Expr> => let [logical & _cmp] in [x y] return {
    let loc = x.here(());
    y.into_iter().rev().fold(x, |i, e| {
        match &e.0 {
            Oprt::Lt => loc.here(Expr::Lt(Box::new(i), Box::new(e.1))),
            Oprt::Gt => loc.here(Expr::Gt(Box::new(i), Box::new(e.1))),
            Oprt::Leq => loc.here(Expr::Leq(Box::new(i), Box::new(e.1))),
            Oprt::Geq => loc.here(Expr::Geq(Box::new(i), Box::new(e.1))),
            Oprt::Eq => loc.here(Expr::Eq(Box::new(i), Box::new(e.1))),
            _ => unreachable!(),
        }
    })
}}

grammar! {_access: Vec<Located<String>> =>
    let [(.Period >> ident & _access) <m> collect | [vec![]]]
    in [x] return { x }
}
grammar! {_index: Vec<Located<Expr>> =>
    let [(.LBracket>> expr <<.RBracket & _index) <m> collect | [vec![]]]
    in [x] return { x }
}
grammar! {_neg_not: Located<Expr> =>
    let [neg_not_op & neg_not] in [x y]
    return {
        let loc = x.here(());
        match x.as_inner() {
            Oprt::Neg => loc.here(Expr::Neg(Box::new(y))),
            Oprt::Not => loc.here(Expr::Not(Box::new(y))),
            _ => unreachable!(),
        }
    }
}
grammar! {_pow: Vec<Located<Expr>> =>
    let [(.Caret>> neg_not & _pow) <m> collect | [vec![]]] in [x] return { x }
}
grammar! {_mul_div_rem: Vec<(Oprt, Located<Expr>)> =>
    let [(mul_div_rem_op & pow & _mul_div_rem) <m> |x y xs| { collect(((x, y), xs)) } | [vec![]]] in [x] return {x}
}
grammar! {_add_sub: Vec<(Oprt, Located<Expr>)> =>
    let [(add_sub_op & mul_div_rem & _add_sub) <m> |x y xs| { collect(((x, y), xs)) } | [vec![]]] in [x] return {x}
}
grammar! {_logical: Vec<(Oprt, Located<Expr>)> =>
    let [(log_op & add_sub & _logical) <m> |x y xs| { collect(((x, y), xs)) } | [vec![]]] in [x] return {x}
}
grammar! {_cmp: Vec<(Oprt, Located<Expr>)> =>
    let [(cmp_op & logical & _cmp) <m> |x y xs| { collect(((x, y), xs)) } | [vec![]]] in [x] return {x}
}

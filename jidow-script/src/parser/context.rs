use super::error::ParseError::{self, *};

pub struct Context {
    error: Option<ParseError>,
}

impl Context {
    pub fn new() -> Self {
        Self { error: None }
    }

    pub(super) fn update_error(&mut self, new_error: ParseError) {
        match &self.error {
            Some(old_error) => match (old_error, &new_error) {
                (UnexpectedToken(t1), UnexpectedToken(t2)) => {
                    let (line1, col1) = t1.location();
                    let (line2, col2) = t2.location();
                    if line2 > line1 || (line2 == line1 && col2 > col1) {
                        self.error = Some(new_error)
                    }
                }
                (_, EndWhileParsing | FailedToParseAll) => self.error = Some(new_error),
                (_, _) => (),
            },
            None => self.error = Some(new_error),
        }
    }

    pub fn error(self) -> Option<ParseError> {
        self.error
    }
}

use crate::Located;
use std::time::Duration;

#[derive(Debug, Clone)]
pub enum Stmt {
    Block(Vec<Located<Stmt>>),
    ForLoop {
        item: Located<String>,
        array: Box<Located<Expr>>,
        body: Box<Located<Stmt>>,
    },
    Condition {
        arms: Vec<(Located<Expr>, Located<Stmt>)>,
        other: Option<Box<Located<Stmt>>>,
    },
    WhileLoop {
        cond: Box<Located<Expr>>,
        body: Box<Located<Stmt>>,
    },
    LetBe(Located<String>, Box<Located<Expr>>),
    SetTo(Located<String>, Box<Located<Expr>>),
    ExprStmt(Box<Located<Expr>>),
    Return(Box<Located<Expr>>),
    Break,
    Continue,
    None,
}

#[derive(Debug, Clone)]
pub enum Expr {
    HexColor(u32),
    Duration(Duration),

    LitInt(u32),
    LitFloat(f64),
    LitStr(String),
    LitArr(Vec<Located<Expr>>),
    Var(String),

    True,
    False,

    Neg(Box<Located<Expr>>),
    Not(Box<Located<Expr>>),

    Access(Box<Located<Expr>>, Located<String>),
    Index(Box<Located<Expr>>, Box<Located<Expr>>),

    Pow(Box<Located<Expr>>, Box<Located<Expr>>),
    Mul(Box<Located<Expr>>, Box<Located<Expr>>),
    Div(Box<Located<Expr>>, Box<Located<Expr>>),
    Rem(Box<Located<Expr>>, Box<Located<Expr>>),
    Add(Box<Located<Expr>>, Box<Located<Expr>>),
    Sub(Box<Located<Expr>>, Box<Located<Expr>>),
    And(Box<Located<Expr>>, Box<Located<Expr>>),
    Or(Box<Located<Expr>>, Box<Located<Expr>>),

    Lt(Box<Located<Expr>>, Box<Located<Expr>>),
    Gt(Box<Located<Expr>>, Box<Located<Expr>>),
    Eq(Box<Located<Expr>>, Box<Located<Expr>>),
    Leq(Box<Located<Expr>>, Box<Located<Expr>>),
    Geq(Box<Located<Expr>>, Box<Located<Expr>>),

    FnSelf,
    Call(Box<Located<Expr>>, Vec<Located<Expr>>),
    Func {
        params: Vec<Located<String>>,
        body: Box<Located<Stmt>>,
    },
}

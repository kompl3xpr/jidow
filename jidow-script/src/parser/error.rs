use crate::{locate::Located, tokenizer::tokens::Token};
use std::fmt::Display;

#[derive(Debug, Clone)]
pub enum ParseError {
    FailedToParseAll,
    EndWhileParsing,
    UnexpectedToken(Located<Token>),
}

impl Display for ParseError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{:?}", self)
    }
}

impl std::error::Error for ParseError {}

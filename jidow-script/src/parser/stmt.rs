use super::expr::expr;
use super::macros::*;
use crate::{
    locate::Located,
    parser::{
        ast::{Expr, Stmt},
        base::*,
        Parse,
    },
    tokens::Keyword::*,
};

grammar! {stmt: Located<Stmt> =>
let [cond|control|ret|let_be|set_to|for_loop|while_loop|block|expr_stmt|none]
in [x] return { x }}

grammar! {none: Located<Stmt> => let [[Stmt::None] & next_ln] in [x loc] return { loc.here(x) }}

grammar! {ret: Located<Stmt> =>
    let [@Return & expr <<next_ln] in [loc res]
    return {
        loc.here(Stmt::Return(Box::new(res)))
    }
}

grammar! {control: Located<Stmt> =>
    let [[Stmt::Break] & @Break <<next_ln  |[Stmt::Continue] & @Continue <<next_ln]
    in [x loc] return { loc.here(x) }
}

grammar! {let_be: Located<Stmt> =>
    let [@Let & ident & (@Be>> expr <<next_ln)] in [loc x y]
    return { loc.here(Stmt::LetBe(x, Box::new(y))) }
}

grammar! {set_to: Located<Stmt> =>
    let [@Set & ident & (@To>> expr <<next_ln)] in [loc x y]
    return { loc.here(Stmt::SetTo(x, Box::new(y))) }
}

grammar! {for_loop: Located<Stmt> =>
    let [@For & ident <<@In & expr & (@Do>> next_ln>> stmt)] in [loc item iter body]
    return {
        loc.here(Stmt::ForLoop { item, array: Box::new(iter), body: Box::new(body) })
    }
}

grammar! {while_loop: Located<Stmt> =>
    let [@While & expr <<@Do <<next_ln & stmt] in [loc cond body]
    return {
        loc.here(Stmt::WhileLoop { cond: Box::new(cond), body: Box::new(body) })
    }
}

grammar! {expr_stmt: Located<Stmt> =>
    let [expr <<next_ln] in [x] return {
        let loc = x.here(());
        loc.here(Stmt::ExprStmt(Box::new(x)))
    }
}

grammar! {cond: Located<Stmt> =>
    let [_cond1 & _cond2] in [x y] return {
        let loc = x.0.here(());
        y.0.push(x);
        y.0.reverse();
        loc.here(Stmt::Condition {
            arms: y.0,
            other: y.1.map(|x| Box::new(x))
        })
    }
}
grammar! {_cond1: (Located<Expr>, Located<Stmt>) =>
    let [@If>> expr <<@Do <<next_ln & stmt] in [x] return { x }
}
grammar! {_cond2: (Vec<(Located<Expr>, Located<Stmt>)>, Option<Located<Stmt>>) =>
    let [@Else>> _cond3 | [(vec![], None)]] in [x]
    return { x }
}
grammar! {_cond3: (Vec<(Located<Expr>, Located<Stmt>)>, Option<Located<Stmt>>) =>
    let [(_cond1 & _cond2) <m> |x y| {y.0.push(x); y}
        |(@Do>> next_ln>> stmt) <m> |x| {(vec![], Some(x))}]
    in [x]
    return { x }
}

grammar! {stmts: Vec<Located<Stmt>> =>
    let [(stmt & stmts) <m> collect | [vec![]]] in [x] return { x }
}
grammar! {expr_block: Located<Stmt> =>
    let [scope_begin & stmts <<scope_end] in [loc stmts]
    return {
        stmts.reverse();
        loc.here(Stmt::Block(stmts))
    }
}

grammar! {block: Located<Stmt> =>
    let [expr_block << (next_ln <m> |x| {x.into_inner()} | [()])] in [x]
    return { x }
}

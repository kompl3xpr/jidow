mod error;
mod interpreter;
mod locate;
mod parser;
mod tokenizer;

use interpreter::Interpreter;
use once_cell::sync::OnceCell;
use parser::ast;
use parser::{error::ParseError, Context, Parse};
use std::sync::mpsc::{sync_channel, Receiver, SyncSender};

pub(crate) use locate::Located;
pub(crate) use parser::StatmentParser;
pub(crate) use tokenizer::{tokens, Tokenizer};

pub use error::ScriptError;

static OUTPUT_SENDER: OnceCell<SyncSender<String>> = OnceCell::new();

pub fn init_output_channel() -> Option<Receiver<String>> {
    let (tx, rx) = sync_channel(32);
    OUTPUT_SENDER.set(tx).ok().map(|_| rx)
}

pub struct SyntaxTree(Located<ast::Stmt>);

pub fn parse(script: &str) -> Result<SyntaxTree, ScriptError> {
    let tokens = Tokenizer::new(script).try_tokenize()?;
    let mut context = Context::new();
    let parsed = StatmentParser.parse(&tokens, &mut context);
    let (ast, rest) = match parsed {
        Some(parsed) => parsed,
        _ => return Err(context.error().unwrap().into()),
    };
    match rest.is_empty() {
        true => Ok(SyntaxTree(ast)),
        _ => Err(ParseError::FailedToParseAll.into()),
    }
}

pub fn run(ast: &SyntaxTree) -> Result<(), ScriptError> {
    Ok(Interpreter::new().exec(&ast.0)?)
}

#![allow(unused)]

use eframe::{
    egui::{self, Button, ScrollArea, SidePanel, Slider, TextEdit, WidgetText},
    epaint::Color32,
    run_native, App, NativeOptions,
};
use jidow_script;
use std::sync::{
    atomic::{AtomicBool, Ordering},
    Arc, Mutex,
};

mod script_mng;
mod ui;

const EXAMPLE: &str = r#"~ Thanks for using Jidow!
~ Here are some example codes!

let i be Cursor.position

for i in range: 0 10000000 do
    if i = 10 do
        break
    Cursor.move: 10 10
    lclick
    sleep: 0h0s500ms
    if i % 2 = 0 do
        continue
    print: fmt: "{}: {}" i Screen.color: Cursor.position

print: "Position:" i ~ defined in Line 4
"#;

struct Jidow {
    repeated: bool,
    times: usize,
    is_running: Arc<AtomicBool>,
    output: Arc<Mutex<String>>,
    selected: String,
    script: String,
    message: Arc<Mutex<String>>,
}

impl Jidow {
    pub fn new(output: Arc<Mutex<String>>) -> Self {
        Self {
            repeated: false,
            times: 1,
            is_running: Arc::new(AtomicBool::new(false)),
            output,
            selected: "Example".into(),
            script: String::from(EXAMPLE),
            message: Arc::new(Mutex::new(String::new())),
        }
    }
}

impl App for Jidow {
    fn update(&mut self, ctx: &eframe::egui::Context, frame: &mut eframe::Frame) {
        self.update_left(ctx, frame);
        self.update_editor(ctx, frame);
    }
}

fn main() {
    let rx = jidow_script::init_output_channel().unwrap();
    let output = Arc::new(Mutex::new(String::new()));
    {
        let output = Arc::clone(&output);
        std::thread::spawn(move || {
            while let Ok(s) = rx.recv() {
                output.lock().unwrap().push_str(&s);
            }
        });
    }
    let app = Jidow::new(output);
    let options = NativeOptions {
        resizable: false,
        ..Default::default()
    };
    run_native("Jidow", options, Box::new(|_| Box::new(app)));
}

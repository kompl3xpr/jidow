use eframe::{
    egui::{self, ScrollArea, Slider, TextEdit, Ui, WidgetText},
    epaint::Color32,
};
use std::sync::atomic::Ordering;

use crate::Jidow;

impl Jidow {
    pub fn update_editor(&mut self, ctx: &eframe::egui::Context, _frame: &mut eframe::Frame) {
        let center = egui::CentralPanel::default();
        center.show(ctx, |ui| {
            let add_code_editor = |ui: &mut Ui| {
                let edit = TextEdit::multiline(&mut self.script)
                    .desired_rows(20)
                    .desired_width(f32::INFINITY)
                    .code_editor();
                ui.add(edit);
            };
            ScrollArea::both()
                .max_height(300.0)
                .max_width(500.0)
                .show(ui, add_code_editor);

            ui.add_space(10.0);
            let add_buttons = |ui: &mut Ui| {
                ui.columns(3, |columns| {
                    let is_running = self.is_running.load(Ordering::Acquire);
                    if !is_running && columns[0].button("Run").clicked() {
                        self.spawn_exec();
                    }
                    if columns[1].button("Clear Output").clicked() {
                        self.output.lock().unwrap().clear();
                    }
                    columns[2].menu_button("Settings", |ui| {
                        ui.checkbox(&mut self.repeated, "Repeated");
                        if !self.repeated {
                            ui.add(Slider::new(&mut self.times, 0..=100).suffix(" times"));
                        }
                    });
                });
            };
            ScrollArea::neither()
                .id_source("buttons")
                .max_width(500.0)
                .show(ui, add_buttons);

            ui.add_space(10.0);
            ScrollArea::both()
                .id_source("output")
                .max_height(100.0)
                .max_width(500.0)
                .show(ui, |ui| {
                    let output = ui.add(
                        TextEdit::multiline(&mut *self.output.lock().unwrap())
                            .desired_rows(5)
                            .desired_width(f32::INFINITY)
                            .code_editor()
                            .interactive(false),
                    );
                });

            ui.add_space(10.0);
            ui.label(WidgetText::from(&*self.message.lock().unwrap()).color(Color32::RED));
        });
    }

    pub fn spawn_exec(&self) {
        self.message.lock().unwrap().clear();
        self.is_running.store(true, Ordering::Relaxed);
        let message = self.message.clone();
        let is_running = self.is_running.clone();
        let script = self.script.clone();
        std::thread::spawn(move || {
            match jidow_script::parse(&script) {
                Err(err) => *message.lock().unwrap() = format!("{}", err),
                Ok(ast) => match jidow_script::run(&ast) {
                    Err(err) => *message.lock().unwrap() = format!("{}", err),
                    Ok(()) => message.lock().unwrap().clear(),
                },
            }
            is_running.store(false, Ordering::Release);
        });
    }
}

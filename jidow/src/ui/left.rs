use eframe::egui::{self, SidePanel};

use crate::Jidow;

impl Jidow {
    pub fn update_left(&mut self, ctx: &eframe::egui::Context, _frame: &mut eframe::Frame) {
        let left = SidePanel::left("left")
            .resizable(false)
            .default_width(150.0);
        left.show(ctx, |ui| {
            egui::global_dark_light_mode_buttons(ui);
            ui.separator();
            ui.label("Scripts:");
            ui.menu_button(self.selected.clone(), |ui| {
                if ui
                    .selectable_label(self.selected == "Example", "Example")
                    .clicked()
                {
                    self.selected = String::from("Example");
                }
                if ui
                    .selectable_label(self.selected == "Others", "Others")
                    .clicked()
                {
                    self.selected = String::from("Others");
                }
            });
        });
    }
}

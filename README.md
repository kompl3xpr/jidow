# `Jidow`

A lightweight and easy-to-use mouse & keyboard automation tool.

## Intended Features (Subject to Change)

- [x] Purely written in Rust
- [ ] Lightweight & simple
- [ ] Script management system
- [ ] Windows, Linux & macOS
- [ ] Official support for Chinese & English
- [ ] Third-Party-friendly localization interface
- [x] GUI editor and executor
- [ ] Customizable hotkey bindings for script execution
